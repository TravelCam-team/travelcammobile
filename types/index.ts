export interface OfflineResType {
  created_at: string;
}

export interface OfflinePhotoType extends OfflineResType {
  uri: string;
}

export interface OfflineDiaryType extends OfflineResType {
  title: string;
  body: string;
  uuid: string;
}

export interface OfflineDiaryTypeWithoutUuid extends OfflineDiaryType {
  title: string;
  body: string;
  uuid: string;
}
