import AsyncStorage from '@react-native-community/async-storage';
import {sortByDate} from '../helpers';
import {OfflinePhotoType} from '../types';
import Auth from './Auth';

class PhotoManager {
  private static instance: PhotoManager | null = null;

  public static getInstance(): PhotoManager {
    if (!PhotoManager.instance) {
      return (PhotoManager.instance = new PhotoManager());
    }

    return PhotoManager.instance;
  }

  private constructor() {}

  public async addOfflinePhotos(newPhotos: Array<OfflinePhotoType>) {
    let photos = await this.getOfflinePhotos();
    const user = await Auth.user();

    if (photos) {
      photos = photos.concat(newPhotos);
    } else {
      photos = newPhotos;
    }

    await AsyncStorage.setItem(
      user.id.toString() + 'offlinePhotos',
      JSON.stringify(photos),
    );
  }

  public async removeOfflinePhotos() {
    const user = await Auth.user();

    await AsyncStorage.removeItem(user.id.toString() + 'offlinePhotos');
  }

  public async removeOfflinePhoto(uri: string) {
    let photos = await this.getOfflinePhotos();
    const user = await Auth.user();

    if (photos) {
      photos = photos.filter(image => image.uri != uri);

      // if (photos.length === 0) {
      //   await AsyncStorage.removeItem(user.id.toString() + 'offlinePhotos');

      //   return;
      // }

      await AsyncStorage.setItem(
        user.id.toString() + 'offlinePhotos',
        JSON.stringify(photos),
      );
    }
  }

  public async getOfflinePhotos(): Promise<Array<OfflinePhotoType> | null> {
    const user = await Auth.user();
    let photos = await AsyncStorage.getItem(
      user.id.toString() + 'offlinePhotos',
    );

    if (photos) {
      return JSON.parse(photos);
    }
    return null;
  }

  public async getLatestOfflinePhotos(): Promise<Array<
    OfflinePhotoType
  > | null> {
    let photos = await this.getOfflinePhotos();

    if (photos) {
      return sortByDate(photos);
    }

    return null;
  }

  public async getLastPhotoUri(): Promise<string | undefined> {
    const photos = await this.getLatestOfflinePhotos();

    if (photos && photos.length) {
      return photos[0].uri;
    }

    return undefined;
  }
}

export default PhotoManager.getInstance();
