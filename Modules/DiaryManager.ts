import AsyncStorage from '@react-native-community/async-storage';
import {sortByDate, generateUuid} from '../helpers';
import {OfflineDiaryType, OfflineDiaryTypeWithoutUuid} from '../types';
import Auth from './Auth';

class DiaryManager {
  private static instance: DiaryManager | null = null;

  public static getInstance(): DiaryManager {
    if (!DiaryManager.instance) {
      return (DiaryManager.instance = new DiaryManager());
    }

    return DiaryManager.instance;
  }

  private constructor() {}

  public async addOfflineDiary(newDiary: OfflineDiaryTypeWithoutUuid) {
    newDiary.uuid = generateUuid();

    let diaries = await this.getOfflineDiaries();
    const user = await Auth.user();

    if (diaries) {
      diaries = diaries.concat(newDiary);
    } else {
      diaries = [newDiary];
    }

    await AsyncStorage.setItem(
      user.id.toString() + 'offlineDiaries',
      JSON.stringify(diaries),
    );
  }

  public async removeOfflineDiaries() {
    const user = await Auth.user();

    await AsyncStorage.removeItem(user.id.toString() + 'offlineDiaries');
  }

  public async getOfflineDiaries(): Promise<Array<OfflineDiaryType> | null> {
    const user = await Auth.user();
    let diaries = await AsyncStorage.getItem(
      user.id.toString() + 'offlineDiaries',
    );

    if (diaries) {
      return JSON.parse(diaries);
    }

    return null;
  }

  public async getOfflineDiary(uuid: string): Promise<OfflineDiaryType | null> {
    let diaries = await this.getOfflineDiaries();

    if (diaries) {
      diaries = diaries.filter(d => d.uuid === uuid);

      if (diaries.length === 1) {
        return diaries[0];
      }
    }

    return null;
  }

  public async removeOfflineDiary(uuid: string) {
    let diaries = await this.getOfflineDiaries();

    if (diaries) {
      diaries = diaries.filter(d => d.uuid !== uuid);

      // if (diaries.length === 0) {
      //   await this.removeOfflineDiaries();

      //   return;
      // }

      const user = await Auth.user();

      await AsyncStorage.setItem(
        user.id.toString() + 'offlineDiaries',
        JSON.stringify(diaries),
      );
    }
  }

  public async updateOfflineDiary(
    uuid: string,
    newDiary: OfflineDiaryTypeWithoutUuid,
  ) {
    let diaries = await this.getOfflineDiaries();

    if (!diaries) {
      return;
    }

    for (let diary of diaries) {
      if (diary.uuid === uuid) {
        diary.title = newDiary.title;
        diary.body = newDiary.body;

        break;
      }
    }

    const user = await Auth.user();

    await AsyncStorage.setItem(
      user.id.toString() + 'offlineDiaries',
      JSON.stringify(diaries),
    );
  }

  public async getLatestOfflineDiaries(): Promise<Array<
    OfflineDiaryType
  > | null> {
    let diaries = await this.getOfflineDiaries();

    if (diaries) {
      return sortByDate(diaries);
    }

    return null;
  }
}

export default DiaryManager.getInstance();
