import * as Keychain from 'react-native-keychain';
import {request} from '../helpers';
import AsyncStorage from '@react-native-community/async-storage';

class Auth {
  private static instance: Auth | null = null;

  private userData: any = null;

  public static getInstance(): Auth {
    if (!Auth.instance) {
      return (Auth.instance = new Auth());
    }

    return Auth.instance;
  }

  private constructor() {}

  // retrieve user data from async storage
  public async user() {
    if (this.userData == null) {
      try {
        const data = await AsyncStorage.getItem('userData');

        if (data) {
          this.userData = JSON.parse(data);
        } else {
          this.userData = null;
        }
      } catch (error) {
        this.userData = null;
        this.throwStorageError();
      }
    }

    return this.userData;
  }

  // set user data in async storage
  public async setUserData(userData: any) {
    this.userData = userData;

    try {
      await AsyncStorage.setItem('userData', JSON.stringify(this.userData));
    } catch (error) {
      this.userData = null;
      this.throwStorageError();
    }
  }

  // clear user data
  public async emptyUserData() {
    this.userData = null;

    try {
      await AsyncStorage.removeItem('userData');
    } catch (error) {
      this.throwStorageError();
    }
  }

  private throwKeychainError() {
    throw new Error('Unable to access keychain.');
  }

  private throwStorageError() {
    throw new Error('Unable to access app storage.');
  }

  // retrieve user's api token
  public async getCredentials() {
    try {
      const credentials = await Keychain.getGenericPassword();

      if (credentials) {
        return credentials;
      }
      return null;
    } catch (error) {
      this.throwKeychainError();
    }
  }

  // persist user data in local memory
  public async logIn(id: number, token: string, userData: any) {
    try {
      await Keychain.setGenericPassword(
        id.toString(),
        id.toString() + '.' + token,
      );

      await this.setUserData(userData);
    } catch (error) {
      this.throwKeychainError();
    }
  }

  public async logOut() {
    try {
      const credentials = await Keychain.getGenericPassword();

      if (credentials) {
        await request('logout', 'post', credentials.password, undefined, false);
      }
    } catch (error) {
      this.throwKeychainError();
    }

    try {
      await Keychain.resetGenericPassword();
    } catch (error) {
      this.throwKeychainError();
    }
  }
}

export default Auth.getInstance();
