import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {Icon} from 'react-native-elements';

import {MenuProvider} from 'react-native-popup-menu';

import AuthScreen from './Components/Screens/Auth/AuthScreen';
import LandingScreen from './Components/Screens/LandingScreen';
import LoginScreen from './Components/Screens/Auth/LoginScreen';
import RegisterScreen from './Components/Screens/Auth/RegisterScreen';
import ForgotPasswordScreen from './Components/Screens/Auth/ForgotPasswordScreen';
import HomeScreen from './Components/Screens/HomeScreen';
import CameraScreen from './Components/Screens/CameraScreen';
import SettingsScreen from './Components/Screens/Dashboard/SettingsScreen';
import DiscoverScreen from './Components/Screens/Dashboard/DiscoverScreen';
import ViewProfileScreen from './Components/Screens/Dashboard/ViewProfileScreen';
import ViewTripScreen from './Components/Screens/Dashboard/ViewTripScreen';
import ViewUniversalTripScreen from './Components/Screens/Dashboard/ViewUniversalTripScreen';
import ViewUniversalProfileScreen from './Components/Screens/Dashboard/ViewUniversalProfileScreen';

import EditDescriptionScreen from './Components/Screens/Dashboard/EditDescriptionScreen';
import MyTripsScreen from './Components/Screens/Dashboard/MyTripsScreen';
import NewTripScreen from './Components/Screens/NewTripScreen';
import EditTripScreen from './Components/Screens/EditTripScreen';
import EndTripLoadingScreen from './Components/Screens/EndTripLoadingScreen';
import DeleteTripLoadingScreen from './Components/Screens/DeleteTripLoadingScreen';
import NewDiaryScreen from './Components/Screens/NewDiaryScreen';
import ViewDiaryScreen from './Components/Screens/ViewDiaryScreen';
import EditDiaryScreen from './Components/Screens/EditDiaryScreen';

import MainTabBar from './Components/MainTabBar';
import StackHeader from './Components/StackHeader';

import navigatorStyles from './StyleSheets/StackNavigator';
import variables from './StyleSheets/variables';

import NetInfo, {NetInfoSubscription} from '@react-native-community/netinfo';

import FlashMessage from 'react-native-flash-message';
import {showMessage} from './helpers';

import admob, {MaxAdContentRating} from '@react-native-firebase/admob';

const MyTripsStack = createStackNavigator(
  {
    MyTrips: MyTripsScreen,
    ViewTrip: ViewTripScreen,
  },
  {
    initialRouteName: 'MyTrips',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: variables.lightGreen,
      },
      headerPressColorAndroid: variables.lightGrey,
      headerTintColor: 'white',
    },
  },
);

const DiscoverStack = createStackNavigator(
  {
    DiscoverScreen: DiscoverScreen,
    ViewUniversalProfile: ViewUniversalProfileScreen,
    ViewUniversalTrip: ViewUniversalTripScreen,
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: variables.lightGreen,
      },
      headerPressColorAndroid: variables.lightGrey,
      headerTintColor: 'white',
    },
  },
);

const DashboardTabs = createMaterialTopTabNavigator(
  {
    Discover: {
      screen: DiscoverStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="flight-takeoff" type="material" color={tintColor} />
        ),
        tabBarLabel: 'Discover',
      },
    },
    MyTrips: {
      screen: MyTripsStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="image" type="material-community" color={tintColor} />
        ),
        tabBarLabel: 'My Trips',
      },
    },
    ViewProfile: {
      screen: ViewProfileScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="person" type="material" color={tintColor} />
        ),
        tabBarLabel: 'Profile',
      },
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="settings" type="material-community" color={tintColor} />
        ),
        tabBarLabel: 'Settings',
      },
    },
  },
  {
    lazy: true,
    swipeEnabled: false,
    initialRouteName: 'MyTrips',
    tabBarOptions: {
      pressColor: variables.lightGrey,
      showIcon: true,
      showLabel: true,
      labelStyle: {fontSize: 8},
      activeTintColor: variables.darkGreen,
      inactiveTintColor: 'grey',
      style: {
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: variables.darkGreen,
        borderTopEndRadius: 100,
        borderTopStartRadius: 100,
      },
    },
  },
);

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    NewTrip: NewTripScreen,
    EditTrip: EditTripScreen,
    NewDiary: NewDiaryScreen,
    ViewDiary: ViewDiaryScreen,
    EditDiary: EditDiaryScreen,
    EditDescription: EditDescriptionScreen,
  },
  {
    defaultNavigationOptions: {
      ...navigatorStyles,
      headerTitle: () => <StackHeader />,
    },
  },
);

const AppNavigator = createMaterialTopTabNavigator(
  {
    Camera: {
      screen: CameraScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="camera-alt" type="material" color={tintColor} />
        ),
      },
    },
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="home" type="material" color={tintColor} />
        ),
      },
    },
    Dashboard: {
      screen: DashboardTabs,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="menu" type="material" color={tintColor} />
        ),
      },
    },
  },
  {
    tabBarComponent: props => {
      return <MainTabBar {...props} />;
    },
    tabBarPosition: 'bottom',
    initialRouteName: 'Home',
    lazy: true,
    swipeEnabled: true,
    tabBarOptions: {
      pressColor: variables.lightGrey,
      showIcon: true,
      showLabel: false,
      activeTintColor: variables.darkGreen,
      inactiveTintColor: 'grey',
      style: {
        elevation: 20,
        shadowColor: 'black',
        backgroundColor: 'white',
      },
      indicatorStyle: {
        backgroundColor: variables.darkGreen,
      },
    },
  },
);

const AuthStack = createStackNavigator(
  {
    Auth: AuthScreen,
    LogIn: LoginScreen,
    Register: RegisterScreen,
    ForgotPassword: ForgotPasswordScreen,
  },
  {
    defaultNavigationOptions: {
      ...navigatorStyles,
      headerTitle: () => <StackHeader />,
    },
  },
);

const App = createAppContainer(
  createSwitchNavigator(
    {
      Landing: {
        screen: LandingScreen,
        path: 'landing/:route/:user_id/:user_token',
        params: {user_id: null, user_token: null},
      },
      App: AppNavigator,
      Auth: AuthStack,
      EndTripLoading: EndTripLoadingScreen,
      DeleteTripLoading: DeleteTripLoadingScreen,
    },
    {
      initialRouteName: 'Landing',
    },
  ),
);

const prefix = 'travelcam://';

export default class MainApp extends React.Component {
  unsubscribe: NetInfoSubscription | null = null;

  state = {
    isConnected: undefined,
  };

  componentDidMount() {
    admob()
      .setRequestConfiguration({
        // Update all future requests suitable for parental guidance
        maxAdContentRating: MaxAdContentRating.T,

        // Indicates that you want your content treated as child-directed for purposes of COPPA.
        tagForChildDirectedTreatment: false,

        // Indicates that you want the ad request to be handled in a
        // manner suitable for users under the age of consent.
        tagForUnderAgeOfConsent: false,
      })
      .then(() => {});

    this.unsubscribe = NetInfo.addEventListener(state => {
      if (this.state.isConnected === undefined) {
        this.setState({
          isConnected: state.isConnected,
        });
      } else if (this.state.isConnected != state.isConnected) {
        this.setState({
          isConnected: state.isConnected,
        });

        if (state.isConnected) {
          showMessage('Online');
        } else {
          showMessage('No internet');
        }
      }
    });
  }

  componentWillUnmount() {
    if (this.unsubscribe) {
      this.unsubscribe();
    }
  }

  render() {
    return (
      <MenuProvider>
        <App uriPrefix={prefix} />

        <FlashMessage position="bottom" />
      </MenuProvider>
    );
  }
}
