import {Alert, StatusBar, Linking} from 'react-native';
import {unlink, exists} from 'react-native-fs';
import {showMessage as libShowMessage} from 'react-native-flash-message';
import variables from './StyleSheets/variables';

import NetInfo from '@react-native-community/netinfo';

import {OfflineResType} from 'types';

export async function deleteFileIfExists(path: string) {
  if (await exists(path)) {
    await unlink(path);
  }
}

export async function request(
  uri: string,
  method?: string,
  body?: Object,
  bearer?: string,
  showAlert?: boolean,
) {
  if (showAlert === undefined) {
    showAlert = true;
  }

  const netState = await NetInfo.fetch();

  if (!netState.isConnected || !netState.isInternetReachable) {
    if (showAlert) {
      Alert.alert(
        'Error',
        'No internet connection. Please try again later.',
        [{text: 'OK', onPress: () => {}}],
        {cancelable: false},
      );
    }

    return;
  }

  const headers = {
    'Content-Type': 'application/json',
    Authorization: bearer ? 'Bearer ' + bearer : '',
  };

  if (!method) {
    method = 'get';
  }

  try {
    const response = await fetch(baseUrl() + uri, {
      method: method,
      headers: headers,
      body: body ? JSON.stringify(body) : undefined,
    });

    return response;
  } catch (error) {
    if (showAlert) {
      Alert.alert(
        'Error',
        'No internet connection. Please try again later.',
        [{text: 'OK', onPress: () => {}}],
        {cancelable: false},
      );
    }

    return null;
  }
}

export function now(): string {
  return new Date().toISOString();
}

export function sortByDate(resources: Array<OfflineResType>) {
  return resources.sort((a, b) => {
    const dateA = new Date(a.created_at);
    const dateB = new Date(b.created_at);

    if (dateA < dateB) {
      return 1;
    }

    if (dateA > dateB) {
      return -1;
    }

    return 0;
  });
}

export function baseUrl() {
  return baseWebUrl() + 'api/';
}

export function baseWebUrl() {
  return 'https://travelcam.org/';
}

export function setWhiteStatusBar() {
  StatusBar.setBarStyle('dark-content');
  StatusBar.setTranslucent(false);
  StatusBar.setBackgroundColor('white');
}

export function setTranslucentStatusBar() {
  StatusBar.setBarStyle('light-content');
  StatusBar.setTranslucent(true);
  StatusBar.setBackgroundColor('transparent');
}

export function showMessage(title: string) {
  libShowMessage({
    message: title,
    titleStyle: {
      fontSize: variables.messageTitleFontSize,
      alignSelf: 'center',
    },
    style: {justifyContent: 'center'},
    backgroundColor: variables.messageBackgroundColor,
  });
}

export function generateUuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

export async function openUrl(url: string, message = "Can't open URL.") {
  const supported = await Linking.canOpenURL(url);

  if (supported) {
    await Linking.openURL(url);
  } else {
    Alert.alert(message);
  }
}

export function getColumnLayout(resources: any[]) {
  // find indices of diary entries

  let columnLayout: Array<number> = [];
  let diaryIndices = [];

  for (let i = 0; i < resources.length; i++) {
    if (resources[i].body) {
      diaryIndices.push(i);
    }
  }

  // handle the case for no diary Indices

  if (!diaryIndices.length) {
    return resources.map(r => 3);
  }

  for (let [i, index] of diaryIndices.entries()) {
    let currentIndex = columnLayout.reduce((a, b) => a + b, 0);

    for (let k = 0; k < Math.floor((index - currentIndex) / 3); k++) {
      columnLayout.push(3);
    }

    if ((index - currentIndex) % 3 != 0) {
      columnLayout.push((index - currentIndex) % 3);
    }

    // push 1 more for the diary entry
    columnLayout.push(1);
  }

  columnLayout.push(3);

  while (columnLayout.length < resources.length) {
    columnLayout.push(3);
  }

  return columnLayout;
}

export function getColumnIndexOfResource(
  columnLayout: Array<number>,
  resIndex: number,
) {
  let index = 0,
    sum = 0;

  while (sum < resIndex && index < columnLayout.length - 1) {
    sum += columnLayout[index];

    index++;
  }

  if (index <= 0) {
    return 0;
  }

  return index - 1;
}
