import variables from '../StyleSheets/variables';

export default {
  spinnerSize: 30,
  spinnerColor: variables.darkGreen,
};
