export default {
  darkGreen: '#40A35B',
  lightGreen: '#4BBF6B',

  light: '#f9f9f9',
  lightGrey: '#ccc',
  grey: 'grey',

  inputIconColor: 'grey',
  inputIconSize: 20,

  landingSpinnerSize: 50,

  messageTitleFontSize: 17,
  messageBackgroundColor: 'rgba(0,0,0,0.7)',

  endTripColor: '#843232',
};
