import {StyleSheet} from 'react-native';
import variables from '../variables';

export default StyleSheet.create({
  titleView: {
    borderBottomWidth: 0.5,
    borderBottomColor: '#E8E8E8',
    padding: 18,
    paddingTop: 0,
  },
  titleText: {
    fontSize: 18,
    color: variables.darkGreen,
  },
  mainView: {backgroundColor: variables.light, flex: 1},
  scrollView: {
    flex: 1,
    padding: 20,
  },
  bodyView: {
    marginBottom: 40,
  },
});
