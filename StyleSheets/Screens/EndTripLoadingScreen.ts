import {StyleSheet} from 'react-native';
import variables from '../variables';

export default StyleSheet.create({
  loadingView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: variables.light,
  },
});
