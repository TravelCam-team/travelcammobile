import {StyleSheet, StatusBar} from 'react-native';
import variables from '../../StyleSheets/variables';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
    justifyContent: 'center',
  },
  controls: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
  },
  controlsTop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: StatusBar.currentHeight ? StatusBar.currentHeight + 20 : 40,
  },
  flashView: {
    position: 'absolute',
    left: 20,
  },
  switchCameraView: {
    position: 'absolute',
    right: 20,
  },
  noCameraView: {
    position: 'absolute',
    alignSelf: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    padding: 20,
  },
  noCameraViewHeading: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 10,
  },
  noCameraViewHeadingText: {
    color: 'white',
    fontSize: 20,
    marginLeft: 5,
  },
  noCameraViewText: {
    color: 'white',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 1,
    backgroundColor: 'transparent',
    borderRadius: 100,
    borderColor: 'white',
    borderWidth: 4,
    padding: 35,
    alignSelf: 'center',
    margin: 40,
  },
  back: {flex: 1, position: 'absolute', right: 20, bottom: 50, color: 'white'},
});
