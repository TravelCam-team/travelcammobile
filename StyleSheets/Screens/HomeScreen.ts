import {StyleSheet} from 'react-native';
import variables from '../variables';

export default StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: variables.light,
  },
});
