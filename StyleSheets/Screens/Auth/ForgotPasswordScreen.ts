import {StyleSheet} from 'react-native';
import variables from '../../variables';

export default StyleSheet.create({
  mainView: {backgroundColor: variables.light, flex: 1},
  scrollView: {
    padding: 20,
  },
  buttonView: {
    margin: 10,
    marginTop: 20,
  },
  errorStyle: {color: 'red'},
});
