import {StyleSheet} from 'react-native';
import variables from '../../variables';

export default StyleSheet.create({
  mainView: {backgroundColor: variables.light, flex: 1},
  scrollView: {
    padding: 20,
  },
  inputView: {
    marginBottom: 20,
  },
  inputIconStyle: {marginRight: 10},
  errorStyle: {color: 'red'},
  buttonView: {
    margin: 10,
    marginTop: 20,
    marginBottom: 40,
  },
});
