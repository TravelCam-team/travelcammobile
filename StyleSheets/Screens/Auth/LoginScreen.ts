import {StyleSheet} from 'react-native';
import variables from '../../variables';

export default StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: variables.light,
  },
  scrollView: {
    padding: 20,
  },
  errorStyle: {color: 'red'},
  buttonView: {
    margin: 10,
    marginTop: 20,
  },
  forgotPasswordView: {marginTop: 10},
  forgotPasswordButtonTitle: {color: variables.lightGreen},
});
