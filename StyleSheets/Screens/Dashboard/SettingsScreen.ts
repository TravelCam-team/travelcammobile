import {StyleSheet} from 'react-native';
import variables from '../../variables';

export default StyleSheet.create({
  mainView: {
    padding: 20,
    flex: 1,
    backgroundColor: variables.light,
  },
});
