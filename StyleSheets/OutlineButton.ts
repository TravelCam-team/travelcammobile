import {StyleSheet} from 'react-native';
import variables from '../StyleSheets/variables';

export default StyleSheet.create({
  button: {
    borderColor: variables.lightGreen,
    borderWidth: 1,
    backgroundColor: 'white',
  },
  disabledStyle: {
    borderColor: variables.lightGreen,
  },
  buttonTitle: {
    marginLeft: 10,
    color: variables.lightGreen,
  },
  loadingProps: {
    color: variables.lightGreen,
  },
});
