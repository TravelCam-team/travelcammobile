import variables from './variables';

export default {
  headerStyle: {
    shadowColor: 'transparent',
    elevation: 1,
  },
  headerPressColorAndroid: variables.lightGrey,
  headerTintColor: variables.darkGreen,
};
