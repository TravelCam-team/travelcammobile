import React, {Component} from 'react';
import {Modal, View, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';
import SmartGallery from 'react-native-smart-gallery';

type MyGalleryProps = {
  canDeletePhoto: boolean;
  index: number;
  visible: boolean;
  galleryVisible: boolean;
  photos: Array<{uri: string}>;
  onRequestClose: () => void;
  onDeletePhoto: () => void;
  onPageSelected: (index: number) => void;
};

export default class MyGallery extends Component<MyGalleryProps> {
  galleryRef: any;

  componentDidMount() {}

  render() {
    return (
      <Modal
        visible={this.props.visible}
        animationType="slide"
        transparent={false}
        onRequestClose={this.props.onRequestClose}>
        <View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'black',
          }}
        />

        <TouchableOpacity
          style={{
            position: 'absolute',
            top: 15,
            left: 15,
            zIndex: 20,
          }}
          onPress={this.props.onRequestClose}>
          <Icon
            color={'white'}
            name="close"
            type="material-community"
            size={35}
          />
        </TouchableOpacity>
        {this.props.canDeletePhoto ? (
          <TouchableOpacity
            style={{
              position: 'absolute',
              top: 15,
              right: 15,
              zIndex: 20,
            }}
            onPress={async () => {
              await this.props.onDeletePhoto();
            }}>
            <Icon
              color={'white'}
              name="delete"
              type="material-community"
              size={35}
            />
          </TouchableOpacity>
        ) : null}

        {this.props.visible && this.props.galleryVisible && (
          <SmartGallery
            ref={a => (this.galleryRef = a)}
            index={this.props.index}
            images={this.props.photos}
            loadMinimal={true}
            loadMinimalSize={2}
            sensitiveScroll={false}
            onPageSelected={(index: number) => this.props.onPageSelected(index)}
          />
        )}
      </Modal>
    );
  }
}
