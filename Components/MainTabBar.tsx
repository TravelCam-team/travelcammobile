import React, {Component} from 'react';
import {Keyboard, EmitterSubscription} from 'react-native';

import {MaterialTopTabBar} from 'react-navigation-tabs';
import {MaterialTabBarProps} from 'node_modules/react-navigation-tabs/lib/typescript/src/types';

export default class MainTabBar extends Component<MaterialTabBarProps> {
  state = {visible: true};

  keyboardDidShowListener: EmitterSubscription | null = null;
  keyboardDidHideListener: EmitterSubscription | null = null;

  componentDidMount = () => {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () =>
      this.setState({visible: false}),
    );
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () =>
      this.setState({visible: true}),
    );
  };

  componentWillUnmount = () => {
    this.keyboardDidShowListener!.remove();
    this.keyboardDidHideListener!.remove();
  };

  render() {
    if (!this.state.visible) {
      return null;
    } else {
      return <MaterialTopTabBar {...this.props} />;
    }
  }
}
