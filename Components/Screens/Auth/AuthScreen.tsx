import React, {Component} from 'react';
import {View, Text, ImageBackground} from 'react-native';
import {Button, Icon} from 'react-native-elements';

import {
  NavigationStackProp,
  NavigationStackOptions,
} from 'react-navigation-stack';

import WhiteStatusBar from '../../WhiteStatusBar';
import variables from '../../../StyleSheets/variables';

type AuthScreenProps = {
  navigation: NavigationStackProp;
};

export default class AuthScreen extends Component<AuthScreenProps> {
  static navigationOptions: NavigationStackOptions = {
    headerTitleContainerStyle: {justifyContent: 'center'},
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: variables.light,
        }}>
        <WhiteStatusBar />

        <ImageBackground
          source={require('../../../img/home-bg.jpg')}
          style={{flex: 1}}>
          <View style={{flex: 1}} />
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              paddingVertical: 10,
              backgroundColor: 'rgba(0,0,0,0.5)',
              marginBottom: 50,
            }}>
            <Text
              style={{
                color: 'white',
                fontWeight: 'bold',
                fontSize: 30,
                textAlign: 'center',
              }}>
              The ultimate travel journal.
            </Text>
          </View>
          <View style={{flex: 4}}>
            <View style={{marginBottom: 20, marginHorizontal: 20}}>
              <Button
                type="outline"
                buttonStyle={{
                  borderColor: variables.darkGreen,
                  borderWidth: 1,
                  backgroundColor: 'white',
                }}
                titleStyle={{
                  marginLeft: 10,
                  color: variables.darkGreen,
                }}
                title="New account"
                onPress={() => this.props.navigation.navigate('Register')}
                icon={
                  <Icon
                    type="material-community"
                    name="account"
                    color={variables.darkGreen}
                  />
                }
              />
            </View>
            <View style={{marginHorizontal: 20}}>
              <Button
                buttonStyle={{backgroundColor: 'rgba(255,255,255,0.9)'}}
                type="outline"
                titleStyle={{marginLeft: 10, color: variables.darkGreen}}
                title="Log In"
                onPress={() => this.props.navigation.navigate('LogIn')}
              />
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
