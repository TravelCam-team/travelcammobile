import React, {Component} from 'react';
import {View, Alert, ScrollView} from 'react-native';

import {Input, Icon} from 'react-native-elements';

import {request} from '../../../helpers';

import {NavigationStackProp} from 'react-navigation-stack';
import styles from '../../../StyleSheets/Screens/Auth/ForgotPasswordScreen';
import variables from '../../../StyleSheets/variables';

import StackHeader from '../../StackHeader';
import WhiteStatusBar from '../../WhiteStatusBar';
import OutlineButton from '../../OutlineButton';

type ForgotPasswordScreenProps = {
  navigation: NavigationStackProp;
};

export default class ForgotPasswordScreen extends Component<
  ForgotPasswordScreenProps
> {
  static navigationOptions = {
    headerTitle: () => <StackHeader title="Forgot Password" />,
  };

  state = {
    email: '',
    errors: {
      email: [''],
    },
    isLoading: false,
  };

  private async onSubmit() {
    this.setState({errors: {email: ['']}, isLoading: true});

    const response = await request('password/forgot', 'post', {
      email: this.state.email,
    });

    if (!response) {
      this.setState({isLoading: false});
      return;
    }

    let responseJson;
    switch (response.status) {
      case 200:
        // all ok

        responseJson = await response.json();

        Alert.alert(
          'Success',
          responseJson.message,
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );

        this.props.navigation.navigate('Landing');
        return;
      case 422:
        // validation errors
        responseJson = await response.json();

        this.setState({
          errors: responseJson.errors,
        });
        break;
      case 403:
        // output message from server
        responseJson = await response.json();

        Alert.alert(
          'Error',
          responseJson.message,
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
        break;
      default:
        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }

    this.setState({isLoading: false, password: ''});
  }

  render() {
    return (
      <View style={styles.mainView}>
        <ScrollView
          style={styles.scrollView}
          keyboardShouldPersistTaps="handled">
          <WhiteStatusBar />
          <View>
            <Input
              placeholder="email"
              onChangeText={email => this.setState({email})}
              value={this.state.email}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors.email ? this.state.errors.email[0] : ''
              }
              autoCapitalize="none"
              autoFocus={true}
              keyboardType="email-address"
              returnKeyType="go"
              onSubmitEditing={() => {
                this.onSubmit();
              }}
              blurOnSubmit={false}
            />
          </View>

          <View style={styles.buttonView}>
            <OutlineButton
              title="Send password reset link"
              icon={
                <Icon
                  name="paper-plane"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={variables.inputIconSize}
                />
              }
              loading={this.state.isLoading}
              onPress={() => this.onSubmit()}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
