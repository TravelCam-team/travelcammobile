import React, {Component} from 'react';
import {View, Alert, ScrollView} from 'react-native';

import {
  Input,
  Icon as DefaultIcon,
  ListItem,
  Text,
} from 'react-native-elements';

import {request} from '../../../helpers';

import {NavigationStackProp} from 'react-navigation-stack';

import OutlineButton from '../../OutlineButton';
import WhiteStatusBar from '../../WhiteStatusBar';
import StackHeader from '../../StackHeader';

import styles from '../../../StyleSheets/Screens/Auth/RegisterScreen';
import variables from '../../../StyleSheets/variables';

type RegisterScreenProps = {
  navigation: NavigationStackProp;
};

const Icon = (props: {name: string}) => {
  return (
    <DefaultIcon
      name={props.name}
      type="font-awesome"
      color={variables.inputIconColor}
      size={variables.inputIconSize}
    />
  );
};

export default class RegisterScreen extends Component<RegisterScreenProps> {
  static navigationOptions = {
    headerTitle: () => <StackHeader title="Create your account" />,
  };

  state = {
    email: '',
    name: '',
    password: '',
    password_confirmation: '',
    errors: {
      email: [''],
      name: [''],
      password: [''],
      password_confirmation: [''],
    },
    isLoading: false,
  };

  private scrollView: any;
  private nameInput: any;
  private passwordInput: any;
  private passwordConfirmationInput: any;

  private async onSubmit() {
    this.setState({
      errors: {
        email: [''],
        name: [''],
        password: [''],
        password_confirmation: [''],
      },
      isLoading: true,
    });

    this.scrollView.scrollToEnd();

    const response = await request('register', 'post', {
      email: this.state.email,
      name: this.state.name,
      password: this.state.password,
      password_confirmation: this.state.password_confirmation,
    });

    if (!response) {
      this.setState({
        password: '',
        password_confirmation: '',
        isLoading: false,
      });

      return;
    }

    let responseJson;

    switch (response.status) {
      case 201:
        // all ok, user was registered

        responseJson = await response.json();

        Alert.alert(
          'Success',
          responseJson.message,
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );

        this.props.navigation.navigate('Landing');
        return;
      case 422:
        // validation errors

        responseJson = await response.json();

        this.setState({
          errors: responseJson.errors,
        });
        break;
      default:
        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }

    this.setState({
      password: '',
      password_confirmation: '',
      isLoading: false,
    });
  }

  render() {
    return (
      <View style={styles.mainView}>
        <ScrollView
          style={styles.scrollView}
          keyboardShouldPersistTaps="handled"
          ref={s => (this.scrollView = s)}>
          <WhiteStatusBar />

          <View style={styles.inputView}>
            <Input
              placeholder="email@example.com"
              label="Enter your email address"
              leftIcon={<Icon name="envelope" />}
              leftIconContainerStyle={styles.inputIconStyle}
              value={this.state.email}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors.email ? this.state.errors.email[0] : ''
              }
              keyboardType="email-address"
              onChangeText={email => this.setState({email})}
              autoCapitalize="none"
              returnKeyType="next"
              onSubmitEditing={() => {
                this.nameInput.focus();
              }}
              blurOnSubmit={false}
              autoFocus={true}
            />
          </View>

          <View style={styles.inputView}>
            <Input
              ref={input => {
                this.nameInput = input;
              }}
              placeholder="myCoolUsername"
              label="Pick a username"
              leftIcon={<Icon name="user" />}
              leftIconContainerStyle={styles.inputIconStyle}
              value={this.state.name}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors.name ? this.state.errors.name[0] : ''
              }
              onChangeText={name => this.setState({name})}
              autoCapitalize="none"
              returnKeyType="next"
              onSubmitEditing={() => {
                this.passwordInput.focus();
              }}
              blurOnSubmit={false}
            />
          </View>

          <View style={styles.inputView}>
            <Input
              ref={input => {
                this.passwordInput = input;
              }}
              placeholder="***********"
              label="Choose a secure password"
              leftIcon={<Icon name="lock" />}
              leftIconContainerStyle={styles.inputIconStyle}
              value={this.state.password}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors.password ? this.state.errors.password[0] : ''
              }
              onChangeText={password => this.setState({password})}
              secureTextEntry={true}
              returnKeyType="next"
              onSubmitEditing={() => {
                this.passwordConfirmationInput.focus();
              }}
              blurOnSubmit={false}
            />
          </View>

          <View style={styles.inputView}>
            <Input
              ref={input => {
                this.passwordConfirmationInput = input;
              }}
              placeholder="***********"
              label="Re-type your password"
              leftIcon={<Icon name="lock" />}
              leftIconContainerStyle={styles.inputIconStyle}
              value={this.state.password_confirmation}
              onChangeText={password_confirmation =>
                this.setState({password_confirmation})
              }
              secureTextEntry={true}
              returnKeyType="go"
              onSubmitEditing={() => this.onSubmit()}
            />
          </View>

          <View style={{}}>
            <Text
              style={{
                textAlign: 'center',
                color: variables.lightGreen,
                marginBottom: 10,
              }}
              h4>
              Free plan
            </Text>
            <ListItem
              containerStyle={{
                backgroundColor: 'transparent',
                paddingVertical: 0,
                paddingBottom: 5,
              }}
              titleStyle={{color: variables.lightGreen}}
              title="Free forever"
              leftAvatar={
                <DefaultIcon
                  name="check"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={15}
                />
              }
            />
            <ListItem
              containerStyle={{
                backgroundColor: 'transparent',
                paddingVertical: 0,
                paddingBottom: 5,
              }}
              titleStyle={{color: variables.lightGreen}}
              title="Built-in cloud storage"
              leftAvatar={
                <DefaultIcon
                  name="check"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={15}
                />
              }
            />
            <ListItem
              containerStyle={{
                backgroundColor: 'transparent',
                paddingVertical: 0,
                paddingBottom: 5,
              }}
              titleStyle={{color: variables.lightGreen}}
              title="Share & download memories"
              leftAvatar={
                <DefaultIcon
                  name="check"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={15}
                />
              }
            />
            <ListItem
              containerStyle={{
                backgroundColor: 'transparent',
                paddingVertical: 0,
                paddingBottom: 5,
              }}
              titleStyle={{color: variables.lightGreen}}
              title="Up to 5 trips"
              leftAvatar={
                <DefaultIcon
                  name="check"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={15}
                />
              }
            />
            <ListItem
              containerStyle={{
                backgroundColor: 'transparent',
                paddingVertical: 0,
                paddingBottom: 5,
              }}
              titleStyle={{color: variables.lightGreen}}
              title="Up to 50 photos per trip"
              leftAvatar={
                <DefaultIcon
                  name="check"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={15}
                />
              }
            />
            <ListItem
              containerStyle={{
                backgroundColor: 'transparent',
                paddingVertical: 0,
                paddingBottom: 5,
              }}
              titleStyle={{color: variables.lightGreen}}
              title="Offline mode"
              leftAvatar={
                <DefaultIcon
                  name="check"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={15}
                />
              }
            />
            <ListItem
              containerStyle={{
                backgroundColor: 'transparent',
                paddingVertical: 0,
                paddingBottom: 5,
              }}
              titleStyle={{color: variables.lightGreen}}
              title="Original quality images"
              leftAvatar={
                <DefaultIcon
                  name="check"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={15}
                />
              }
            />
          </View>

          <View style={styles.buttonView}>
            <OutlineButton
              icon={
                <DefaultIcon
                  name="check"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={variables.inputIconSize}
                />
              }
              title="Create account!"
              onPress={() => this.onSubmit()}
              loading={this.state.isLoading}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
