import React, {Component} from 'react';
import {View, Alert, ScrollView} from 'react-native';

import {NavigationStackProp} from 'react-navigation-stack';

import {Input, Icon, Button} from 'react-native-elements';

import {request, showMessage} from '../../../helpers';
import Auth from '../../../Modules/Auth';

import WhiteStatusBar from '../../WhiteStatusBar';
import OutlineButton from '../../OutlineButton';

import StackHeader from '../../StackHeader';
import variables from '../../../StyleSheets/variables';
import styles from '../../../StyleSheets/Screens/Auth/LoginScreen';

type LoginScreenProps = {
  navigation: NavigationStackProp;
};

export default class LoginScreen extends Component<LoginScreenProps> {
  state = {
    email: '',
    password: '',
    errors: {
      email: [''],
      password: [''],
    },
    isLoading: false,
  };

  private passwordInput: any;

  static navigationOptions = {
    headerTitle: () => <StackHeader title="Log In" />,
  };

  private async onSubmit() {
    this.setState({errors: {email: [''], password: ['']}, isLoading: true});

    const response = await request('login', 'post', {
      email: this.state.email,
      password: this.state.password,
    });

    if (!response) {
      this.setState({password: '', isLoading: false});
      return;
    }

    let responseJson;

    switch (response.status) {
      case 200:
        // all ok, log in

        responseJson = await response.json();

        await Auth.logIn(
          responseJson.user.id,
          responseJson.user.token,
          responseJson.user.data,
        );

        showMessage('Welcome back');

        this.props.navigation.navigate('Home');
        return;
      case 422:
        // validation errors
        responseJson = await response.json();

        this.setState({
          errors: responseJson.errors,
        });
        break;
      case 403:
        // Output messsage from server
        responseJson = await response.json();

        Alert.alert(
          'Error',
          responseJson.message,
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
        break;
      default:
        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }

    this.setState({password: '', isLoading: false});
  }

  render() {
    return (
      <View style={styles.mainView}>
        <ScrollView
          style={styles.scrollView}
          keyboardShouldPersistTaps="handled">
          <WhiteStatusBar />

          <View>
            <Input
              placeholder="email"
              onChangeText={email => this.setState({email})}
              value={this.state.email}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors.email ? this.state.errors.email[0] : ''
              }
              autoCapitalize="none"
              autoFocus={true}
              keyboardType="email-address"
              returnKeyType="next"
              onSubmitEditing={() => {
                this.passwordInput.focus();
              }}
              blurOnSubmit={false}
            />

            <Input
              ref={input => {
                this.passwordInput = input;
              }}
              placeholder="password"
              value={this.state.password}
              secureTextEntry={true}
              onChangeText={password => this.setState({password})}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors.password ? this.state.errors.password[0] : ''
              }
              returnKeyType="go"
              onSubmitEditing={() => this.onSubmit()}
            />
          </View>

          <View style={styles.buttonView}>
            <OutlineButton
              title="Log In"
              icon={
                <Icon
                  name="unlock-alt"
                  type="font-awesome"
                  color={variables.lightGreen}
                  size={variables.inputIconSize}
                />
              }
              loading={this.state.isLoading}
              onPress={() => this.onSubmit()}
            />
          </View>

          <View style={styles.forgotPasswordView}>
            <Button
              title="Forgot password?"
              type="clear"
              titleStyle={styles.forgotPasswordButtonTitle}
              onPress={() => this.props.navigation.navigate('ForgotPassword')}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
