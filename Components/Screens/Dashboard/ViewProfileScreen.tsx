import React, {Component} from 'react';
import {View, Text, Alert, Image} from 'react-native';
import {Icon, Button, ListItem} from 'react-native-elements';

import {NavigationTabProp} from 'react-navigation-tabs';
import variables from '../../../StyleSheets/variables';
import styles from '../../../StyleSheets/Screens/Dashboard/ViewProfileScreen';
import {baseUrl, showMessage} from '../../../helpers';

import ImagePicker from 'react-native-image-picker';

import {NavigationEventSubscription} from 'react-navigation';

import PageSpinner from '../../../Components/PageSpinner';
import TripListItem from '../../../Components/TripListItem';

import Auth from '../../../Modules/Auth';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';

type ViewProfileScreenProps = {
  navigation: NavigationTabProp;
};

export default class ViewProfileScreen extends Component<
  ViewProfileScreenProps
> {
  state = {
    user: undefined,
    isLoading: false,
  };

  constructor(props: ViewProfileScreenProps) {
    super(props);

    this.renderItem = this.renderItem.bind(this);
  }

  private willFocusListener: NavigationEventSubscription | null = null;

  async componentDidMount() {
    this.willFocusListener = this.props.navigation.addListener(
      'willFocus',
      async () => {
        await this.performRefresh();
      },
    );

    await this.performRefresh();
  }

  componentWillUnmount() {
    this.willFocusListener!.remove();
  }

  async performRefresh() {
    this.setState({isLoading: true});
    const user = await Auth.user();

    this.setState({user, isLoading: false});
  }

  changeProfilePicture() {
    const options = {
      title: 'Select Profile Picture',
      mediaType: 'photo',
      cameraType: 'front',
      storageOptions: {
        skipBackup: true,
        privateDirectory: true,
      },
    };

    ImagePicker.showImagePicker(options, async response => {
      if (!response.didCancel && !response.error) {
        this.setState({isLoading: true});
        await this.uploadPicture(response.uri);

        await this.performRefresh();
        this.setState({isLoading: false});
      }
    });
  }

  async uploadPicture(uri: string) {
    let body = new FormData();
    body.append('photo', {
      uri: uri,
      name: uri,
      type: 'image/jpg',
    });

    const user = await Auth.getCredentials();

    if (!user) {
      return;
    }
    let response;
    try {
      response = await fetch(baseUrl() + 'user/setProfilePicture', {
        method: 'post',
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: 'Bearer ' + user.password,
        },
        body: body,
      });
    } catch (error) {
      Alert.alert(
        'Error',
        'No internet connection. Please try again later.',
        [{text: 'OK', onPress: () => {}}],
        {cancelable: false},
      );
      return;
    }

    let responseJson;

    switch (response.status) {
      case 201:
        // all OK, picture was uploaded
        responseJson = await response.json();

        await Auth.setUserData(responseJson.user);

        break;
      case 422:
        // validation issues
        responseJson = await response.json();

        if (responseJson.errors && responseJson.errors.photo) {
          Alert.alert(
            'Error',
            responseJson.errors.photo[0],
            [{text: 'OK', onPress: () => {}}],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            'Error',
            'There was an error processing your photo.',
            [{text: 'OK', onPress: () => {}}],
            {cancelable: false},
          );
        }
        break;
      default:
        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }
  }

  private renderItem(itemObj: any) {
    const item = itemObj.item;

    return (
      <TripListItem
        item={item}
        onPress={() => {
          this.props.navigation.navigate({
            routeName: 'ViewTrip',
            params: {tripId: item.id},
          });
        }}
      />
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={[styles.mainView, {paddingTop: 20}]}>
          <PageSpinner />
        </View>
      );
    }

    return (
      <View style={styles.mainView}>
        {this.state.user ? (
          <FlatList
            removeClippedSubviews={true}
            data={this.state.user.ended_trips}
            renderItem={this.renderItem}
            keyExtractor={item => item.ended_at}
            ListEmptyComponent={<ListItem title="No finished trips yet" />}
            ListHeaderComponent={
              <>
                <View style={{flex: 1, flexDirection: 'row', marginTop: 20}}>
                  <View
                    style={{flex: 1, marginRight: 10, alignItems: 'center'}}>
                    <TouchableOpacity
                      style={{width: 75, height: 75}}
                      onPress={() => this.changeProfilePicture()}>
                      <FastImage
                        style={{
                          borderRadius: 100,
                          width: '100%',
                          height: '100%',
                        }}
                        source={{
                          uri: this.state.user.profile_picture_permalink,
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      flex: 2,
                      justifyContent: 'center',
                    }}>
                    <Text style={{fontSize: 25}}>{this.state.user.name}</Text>
                  </View>
                </View>
                <View style={{flex: 5, marginTop: 10}}>
                  {this.state.user.description ? (
                    <View
                      style={{
                        backgroundColor: 'white',
                        padding: 10,
                        marginHorizontal: 15,
                        marginBottom: 10,
                      }}>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate('EditDescription');
                        }}>
                        <Text style={{color: variables.grey, fontSize: 15}}>
                          {this.state.user.description}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <Button
                      type="clear"
                      titleStyle={{
                        color: variables.lightGreen,
                        marginLeft: 10,
                      }}
                      onPress={() => {
                        this.props.navigation.navigate('EditDescription');
                      }}
                      title="Add Description"
                      icon={
                        <Icon
                          name="edit"
                          type="material"
                          color={variables.lightGreen}
                        />
                      }
                    />
                  )}

                  <View
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      marginHorizontal: 20,
                    }}>
                    <Text style={{fontSize: 20}}>
                      Trips{' '}
                      {this.state.user.ended_trips
                        ? '(' + this.state.user.ended_trips.length + ')'
                        : '(0)'}
                    </Text>
                  </View>
                </View>
              </>
            }
          />
        ) : null}
      </View>
    );
  }
}
