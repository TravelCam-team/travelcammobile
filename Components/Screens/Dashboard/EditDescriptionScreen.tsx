import React, {Component} from 'react';
import {
  ScrollView,
  View,
  Alert,
  BackHandler,
  NativeEventSubscription,
} from 'react-native';
import {Icon, Input, Button} from 'react-native-elements';
import variables from '../../../StyleSheets/variables';
import {NavigationStackProp} from 'react-navigation-stack';

import styles from '../../../StyleSheets/Screens/Dashboard/EditDescriptionScreen';

import {request} from '../../../helpers';
import PageSpinner from '../../PageSpinner';
import StackHeader from '../../StackHeader';
import Auth from '../../../Modules/Auth';

type EditDescriptionScreenProps = {
  navigation: NavigationStackProp;
};

export default class EditDescriptionScreen extends Component<
  EditDescriptionScreenProps
> {
  backHandler: NativeEventSubscription | null = null;

  static navigationOptions = ({navigation}) => {
    return {
      tabBarVisible: false,
      headerTitle: () => <StackHeader title="New Description" />,
      headerLeft: () => (
        <Button
          buttonStyle={{backgroundColor: 'transparent', marginLeft: 10}}
          onPress={async () => {
            const func = navigation.getParam('goBack');
            await func();
          }}
          icon={
            <Icon
              color={variables.darkGreen}
              name="close"
              type="material-community"
              size={27}
            />
          }
        />
      ),
      headerRight: () => (
        <Button
          buttonStyle={{
            backgroundColor: 'transparent',
            marginRight: 10,
          }}
          onPress={async () => {
            const func = navigation.getParam('onSubmit');
            await func();
          }}
          icon={
            <Icon
              color={variables.darkGreen}
              name="check"
              type="material-community"
              size={27}
            />
          }
        />
      ),
    };
  };

  state = {
    isLoading: false,
    description: '',
    errors: {description: []},
  };

  async componentDidMount() {
    this.setState({isLoading: true});

    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.goBack,
    );

    this.props.navigation.setParams({
      onSubmit: this.onSubmit,
      goBack: this.goBack,
    });

    const user = await Auth.user();
    this.setState({description: user.description, isLoading: false});
  }

  componentWillUnmount() {
    this.backHandler!.remove();
    this.props.navigation.setParams({onSubmit: undefined, goBack: undefined});
  }

  onSubmit = async () => {
    this.setState({
      errors: {
        description: [''],
      },
      isLoading: true,
    });

    const credentials = await Auth.getCredentials();

    if (!credentials) {
      return;
    }

    const response = await request(
      'user/setDescription',
      'post',
      {
        description: this.state.description,
      },
      credentials.password,
    );

    if (!response) {
      this.setState({
        isLoading: false,
      });

      return;
    }

    let responseJson,
      hasErrors = false;

    switch (response.status) {
      case 201:
        // all ok, description was updated
        responseJson = await response.json();

        await Auth.setUserData(responseJson.user);

        break;
      case 422:
        // validation errors
        responseJson = await response.json();

        this.setState({
          errors: responseJson.errors,
        });

        hasErrors = true;
        break;
      default:
        hasErrors = true;

        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }

    this.setState({
      isLoading: false,
    });

    if (hasErrors) {
      return;
    }

    this.props.navigation.pop();
    this.props.navigation.navigate('ViewProfile');
  };

  goBack = async () => {
    this.props.navigation.pop();
    this.props.navigation.navigate('ViewProfile');
  };

  render() {
    return (
      <View style={styles.mainView}>
        {this.state.isLoading ? (
          <PageSpinner style={{marginTop: 20}} />
        ) : (
          <ScrollView
            style={styles.scrollView}
            keyboardShouldPersistTaps="handled">
            <View style={[styles.inputView]}>
              <Input
                autoFocus={true}
                placeholder="A little bit about me..."
                label="Description"
                value={this.state.description}
                errorStyle={styles.errorStyle}
                errorMessage={
                  this.state.errors['description']
                    ? this.state.errors['description'][0]
                    : ''
                }
                onChangeText={description => this.setState({description})}
                returnKeyType="none"
                multiline={true}
                inputContainerStyle={{
                  borderBottomWidth: 0,
                }}
                inputStyle={{textAlignVertical: 'top'}}
                numberOfLines={10}
              />
            </View>
          </ScrollView>
        )}
      </View>
    );
  }
}
