import React, {Component} from 'react';
import {View, Text, Alert} from 'react-native';
import {ListItem} from 'react-native-elements';

import {NavigationTabProp} from 'react-navigation-tabs';
import variables from '../../../StyleSheets/variables';
import styles from '../../../StyleSheets/Screens/Dashboard/ViewUniversalProfileScreen';
import {request} from '../../../helpers';

import PageSpinner from '../../../Components/PageSpinner';
import OfflineView from '../../../Components/OfflineView';
import TripListItem from '../../../Components/TripListItem';

import Auth from '../../../Modules/Auth';
import {FlatList} from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';

type ViewUniversalProfileScreenProps = {
  navigation: NavigationTabProp;
};

export default class ViewUniversalProfileScreen extends Component<
  ViewUniversalProfileScreenProps
> {
  state = {
    user: undefined,
    userId: undefined,
    isLoading: false,
    offline: false,
  };

  static navigationOptions = {
    title: 'View Profile',
  };

  constructor(props: ViewUniversalProfileScreenProps) {
    super(props);

    this.renderItem = this.renderItem.bind(this);
  }

  async componentDidMount() {
    let userId = this.props.navigation.getParam('userId');

    this.setState({userId});

    await this.performRefresh();
  }

  componentWillUnmount() {}

  async performRefresh() {
    this.setState({isLoading: true});

    const credentials = await Auth.getCredentials();

    if (!credentials) {
      return;
    }

    this.setState({password: credentials.password});

    const response = await request(
      'user/show/' + this.state.userId,
      'get',
      undefined,
      credentials.password,
      false,
    );

    if (!response) {
      this.setState({offline: true});

      return;
    }

    let user;

    switch (response.status) {
      case 200:
        // all ok, userdata has loaded
        const responseJson = await response.json();

        user = responseJson;

        this.setState({
          offline: false,
        });

        break;
      default:
        this.setState({offline: true});
        user = undefined;

        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }

    this.setState({user, isLoading: false});
  }

  private renderItem(itemObj: any) {
    const item = itemObj.item;

    return (
      <TripListItem
        item={item}
        onPress={() => {
          this.props.navigation.navigate({
            routeName: 'ViewUniversalTrip',
            params: {tripId: item.id},
          });
        }}
      />
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={[styles.mainView, {paddingTop: 15}]}>
          <PageSpinner />
        </View>
      );
    }

    if (this.state.offline) {
      return (
        <OfflineView
          onPress={() => this.performRefresh()}
          title={'Unable to load profile'}
        />
      );
    }

    return (
      <View style={styles.mainView}>
        {this.state.user ? (
          <FlatList
            removeClippedSubviews={true}
            data={this.state.user.trips}
            renderItem={this.renderItem}
            keyExtractor={item => item.ended_at}
            ListEmptyComponent={<ListItem title="No finished trips yet" />}
            ListHeaderComponent={
              <>
                <View style={{flex: 1, flexDirection: 'row', marginTop: 20}}>
                  <View
                    style={{
                      flex: 1,
                      marginRight: 10,

                      alignItems: 'center',
                    }}>
                    <View style={{width: 75, height: 75}}>
                      <FastImage
                        style={{
                          borderRadius: 100,
                          width: '100%',
                          height: '100%',
                        }}
                        source={{
                          uri: this.state.user.profile_picture_permalink,
                        }}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 2,
                      justifyContent: 'center',
                    }}>
                    <Text style={{fontSize: 25}}>{this.state.user.name}</Text>
                  </View>
                </View>
                <View style={{flex: 5, marginTop: 10}}>
                  {this.state.user.description ? (
                    <View
                      style={{
                        backgroundColor: 'white',
                        padding: 10,
                        marginHorizontal: 15,
                        marginBottom: 10,
                      }}>
                      <Text style={{color: variables.grey, fontSize: 15}}>
                        {this.state.user.description}
                      </Text>
                    </View>
                  ) : null}

                  <View
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      marginHorizontal: 20,
                    }}>
                    <Text style={{fontSize: 20}}>
                      Trips{' '}
                      {this.state.user.trips
                        ? '(' + this.state.user.trips.length + ')'
                        : '(0)'}
                    </Text>
                  </View>
                </View>
              </>
            }
          />
        ) : null}
      </View>
    );
  }
}
