import React, {Component} from 'react';
import {
  View,
  Alert,
  TouchableOpacity,
  Dimensions,
  RefreshControl,
  ScrollView,
  Switch,
  StatusBar,
  Image,
} from 'react-native';
import {Icon, Overlay, Text, ListItem, Input} from 'react-native-elements';

import Clipboard from '@react-native-community/clipboard';

import VariableColumnList from '../../VariableColumnList';

import variables from '../../../StyleSheets/variables';
import styles from '../../../StyleSheets/Screens/Dashboard/ViewTripScreen';
import {
  baseUrl,
  setWhiteStatusBar,
  request,
  getColumnIndexOfResource,
  getColumnLayout,
  baseWebUrl,
  showMessage,
} from '../../../helpers';

import PageSpinner from '../../../Components/PageSpinner';
import Gallery from '../../../Components/Gallery';
import OfflineView from '../../../Components/OfflineView';

import Auth from '../../../Modules/Auth';
import FastImage from 'react-native-fast-image';
import {NavigationEventSubscription} from 'react-navigation';
import {NavigationStackProp} from 'react-navigation-stack';

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import OutlineButton from '../../../Components/OutlineButton';

type ViewTripScreenProps = {
  navigation: NavigationStackProp;
};

export default class ViewTripScreen extends Component<ViewTripScreenProps> {
  state = {
    trip: undefined,
    isLoading: false,
    tripId: undefined,
    private: false,
    password: undefined,
    switchLoading: false,
    galleryModalVisible: false,
    galleryIndex: 0,
    columnLayout: [3],
    resources: [],
    photos: [],
    isFocused: false,
    diaryModalVisible: false,
    diaryLoading: true,
    offline: false,
    shareOverlayVisible: false,
    pinLoading: true,
    pin: undefined,
  };

  private photoDim:any;

  static navigationOptions = ({navigation}) => {
    return {
      title: 'View Trip',

      headerRight: () => (
        <View style={{marginRight: 15}}>
          <Menu>
            <MenuTrigger>
              <Icon color={'white'} name="dots-vertical" type="material-community" size={25} />
            </MenuTrigger>
            <MenuOptions
              customStyles={{
                optionText: {fontSize: 15},
                optionWrapper: {padding: 10},
              }}>
              <MenuOption
                onSelect={async () => {
                  const func = navigation.getParam('onShare');
                  await func();
                }}
                text="Copy shareable link"
              />
              <MenuOption
                onSelect={async () => {
                  const func = navigation.getParam('onDelete');
                  await func();
                }}
                text="Delete trip"
              />
            </MenuOptions>
          </Menu>
        </View>
      ),
    };
  };

  private willFocusListener: NavigationEventSubscription | null = null;
  private willBlurListener: NavigationEventSubscription | null = null;
  private didFocusListener: NavigationEventSubscription | null = null;
  private didBlurListener: NavigationEventSubscription | null = null;

  listRef: any;

  constructor(props: ViewTripScreenProps) {
    super(props);

    this.renderItem = this.renderItem.bind(this);
    this.onShare = this.onShare.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  async componentDidMount() {
    this.photoDim = (Dimensions.get('window').width-30)/3;
    
    this.props.navigation.setParams({
      onShare: this.onShare,
      onDelete: this.onDelete
    });

    this.didFocusListener = this.props.navigation.addListener(
      'didFocus',
      () => {
        setWhiteStatusBar();
      },
    );

    this.didBlurListener = this.props.navigation.addListener('didBlur', () => {
      this.setState({isFocused: false, resources: [], photos: []});
    });

    this.willBlurListener = this.props.navigation.addListener(
      'willBlur',
      () => {
        // this.props.navigation.pop();
      },
    );

    this.willFocusListener = this.props.navigation.addListener(
      'willFocus',
      async () => {
        this.setState({isLoading: true, isFocused: true});
        const tripId = this.props.navigation.getParam('tripId');

        this.setState({tripId});

        await this.performRefresh();

        this.setState({isLoading: false});
      },
    );
  }

  componentWillUnmount() {
    this.willFocusListener!.remove();
    this.willBlurListener!.remove();
    this.didFocusListener!.remove();
    this.didBlurListener!.remove();

    this.props.navigation.setParams({tripId: undefined});
  }

  private async onDelete() {
    if (!this.state.tripId || !this.state.trip) {
      return;
    }

    const deleteConfirmed = async () => {
      this.setState({isLoading: true});

      const response = await request(
        'trip/' + this.state.tripId + '/delete',
        'post',
        undefined,
        this.state.password,
      );
  
      if (!response) {
        this.setState({isLoading: false});
        return;
      }
  
      let responseJson;
      switch (response.status) {
        case 200:
          // all ok, trip was deleted
          responseJson = await response.json();
          await Auth.setUserData(responseJson.user);
          showMessage(responseJson.message);
          this.props.navigation.pop();

          return;
        default:
          console.log(await response.text());
          Alert.alert(
            'Error',
            'Unexpected error ocurred',
            [{text: 'OK', onPress: () => {}}],
            {cancelable: false},
          );
      }

      this.setState({isLoading: false});
    };

    Alert.alert(
      'Confirm',
      'Delete the trip and all associated memories?',
      [
        {text:'Cancel', onPress: () => {}},
        {text: 'Yes', onPress: async () => {await deleteConfirmed()}},
      ],
      {cancelable: true},
    );
  }

  private async onShare() {
    if (!this.state.tripId || !this.state.trip) {
      return;
    }

    if (this.state.private) {
      this.setState({shareOverlayVisible: true, pinLoading: true});

      const credentials = await Auth.getCredentials();

      if (!credentials) {
        return;
      }

      const response = await request(
        'trip/' + this.state.tripId + '/getPin',
        'post',
        undefined,
        credentials.password,
        true,
      );

      if (!response) {
        return;
      }

      switch (response.status) {
        case 200:
          const responseJson = await response.json();

          this.setState({pin: responseJson.pin});

          break;
        default:
          Alert.alert(
            'Error',
            'An unexpected error ocurred. Please try again later.',
            [{text: 'OK', onPress: () => {}}],
            {cancelable: false},
          );
      }

      this.setState({pinLoading: false});
    }

    const url = baseWebUrl() + 'trip/' + this.state.tripId;

    Clipboard.setString(url);

    showMessage('Link copied to clipboard');
  }

  private loadDiary(id: number) {
    const diary = this.state.resources.filter((d) => {
      return d.id === id && d.body;
    });

    if (diary.length == 1) {
      this.setState({diary: diary[0]});
    } else {
      this.setState({diary: undefined});
    }
  }

  private getPhotoSource(item: any) {
    return {
      uri: baseUrl() + 'photo/show/' + item.id,
      method: 'get',
      headers: {
        Authorization: 'Bearer ' + this.state.password,
      },
    };
  }

  private getThumbnailPhotoSource(item: any) {
    return {
      uri: baseUrl() + 'photo/showThumbnail/' + item.id,
      method: 'get',
      headers: {
        Authorization: 'Bearer ' + this.state.password,
      },
    };
  }

  async performRefresh() {
    const credentials = await Auth.getCredentials();

    if (!credentials) {
      return;
    }

    this.setState({password: credentials.password});

    const response = await request(
      'trip/show/' + this.state.tripId,
      'get',
      undefined,
      credentials.password,
      false,
    );

    if (!response) {
      this.setState({offline: true});

      return;
    }

    switch (response.status) {
      case 200:
        // all ok, trip has loaded
        const responseJson = await response.json();

        let resources = responseJson.resources;
        let photos = resources.filter((p) => !p.body);

        photos = photos?.map((value, index) => {
          value.photoIndex = index;
          return value;
        });

        for (let [index, res] of resources.entries()) {
          if (res.photoIndex) {
            photos[res.photoIndex].resourceIndex = index;
          }
        }

        let photoSources = photos?.map((value) => {
          const source = this.getPhotoSource(value);

          return {
            source: source,
            width: '1000',
            height: '1000',
            resourceIndex: value.resourceIndex,
          };
        });

        this.setState({
          offline: false,
          photos: photoSources,
          resources: resources,
          private: responseJson.trip.private,
          trip: responseJson.trip,
        });

        this.setColumnLayout();

        break;
      default:
        this.setState({offline: true});

        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }
  }

  private setColumnLayout() {
    this.setState({columnLayout: getColumnLayout(this.state.resources)});
  }

  private async togglePublic() {
    if (!this.state.tripId) {
      return;
    }

    this.setState({private: !this.state.private});

    if (!this.state.password) {
      return;
    }

    const response = await request(
      'trip/' + this.state.tripId + '/togglePublic',
      'post',
      undefined,
      this.state.password,
    );

    if (!response) {
      this.setState({private: !this.state.private});

      return;
    }

    switch (response.status) {
      case 200:
        // all ok, trip was updated

        return;
      default:
        this.setState({private: !this.state.private});

        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }
  }

  private showGallery() {
    this.setState({galleryModalVisible: true});
    StatusBar.setHidden(true);
  }

  private hideGallery() {
    this.setState({galleryModalVisible: false});

    this.listRef.scrollToIndex({
      index: getColumnIndexOfResource(
        this.state.columnLayout,
        this.state.photos[this.state.galleryIndex].resourceIndex,
      ),
      animated: false,
    });

    StatusBar.setHidden(false);
  }

  private openDiary(id: number) {
    this.setState({
      diaryModalVisible: true,
      diaryLoading: true,
    });

    this.loadDiary(id);

    this.setState({diaryLoading: false});
  }

  private renderItem(itemObj: any) {
    const item = itemObj.item;

    if (!item.body) {
      return (
        <View
          style={{
            flex: 1,
            marginBottom: 10,
          }}>
          <TouchableOpacity
            style={{
              width: this.photoDim,
              height: this.photoDim,
              alignSelf: 'center',
            }}
            onPress={() => {
              this.setState({galleryIndex: item.photoIndex});
              this.showGallery();
            }}>
            <FastImage
              style={{
                width: '100%',
                height: '100%',
                borderRadius: 5,
              }}
              source={this.getThumbnailPhotoSource(item)}
            />
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            marginHorizontal: 5,
            marginBottom: 10,
          }}>
          <ListItem
            Component={TouchableOpacity}
            containerStyle={{
              borderRadius: 5,
              borderWidth: 1,
              borderColor: variables.darkGreen,
            }}
            onPress={() => this.openDiary(item.id)}
            title={item.title}
            titleProps={{numberOfLines:1}}
            subtitle={
              item.body
            }
            subtitleProps={{
              numberOfLines: 3
            }}
            leftAvatar={
              <Icon
                name="book-outline"
                type="material-community"
                color={variables.darkGreen}
              />
            }
            chevron={{color: variables.darkGreen, size: 25}}
          />
        </View>
      );
    }
  }

  render() {
    if (!this.state.isFocused) {
      return <View style={styles.mainView} />;
    }

    if ((!this.state.isLoading && !this.state.trip) || this.state.offline) {
      return (
        <View style={styles.mainView}>
          <OfflineView
            onPress={async () => {
              this.setState({isLoading: true});
              await this.performRefresh();
              this.setState({isLoading: false});
            }}
            title="Unable to load trip"
          />
        </View>
      );
    }

    return (
      <View style={styles.mainView}>
        {this.state.isLoading ? (
          <PageSpinner style={{marginTop:10}} />
        ) : (
          <View style={{flex: 1}}>
            <VariableColumnList
              ref={(list) => (this.listRef = list)}
              initialNumToRender={5}
              windowSize={11}
              removeClippedSubviews={true}
              columnLayout={this.state.columnLayout}
              data={this.state.resources}
              ListEmptyComponent={
                <View
                  style={{
                    backgroundColor: variables.lightGreen,
                    borderRadius: 5,
                    padding: 10,
                    marginBottom: 10,
                    marginHorizontal:10
                  }}>
                  <Text style={{fontSize: 20, color: 'white'}}>
                    No memories
                  </Text>
                </View>
              }
              renderItem={this.renderItem}
              keyExtractor={(item) => item.taken_at + item.id}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={async () => {
                    this.setState({isLoading: true});
                    await this.performRefresh();
                    this.setState({isLoading: false});
                  }}
                />
              }
              ListHeaderComponent={
                <View style={{flexDirection: 'row', marginVertical:5, marginHorizontal:10}}>
                  <View style={{flex: 5}}>
                    <Text h3>{this.state.trip.name}</Text>
                    <Text style={{marginBottom: 10}}>
                      {this.state.trip.location}
                    </Text>
                  </View>
                  <View style={{flex: 1, alignItems:'flex-end'}}>
                    <Text
                      style={{
                        fontSize: 17,
                        color: this.state.private
                          ? 'black'
                          : variables.darkGreen,
                      }}>
                      Public
                    </Text>
                    <Switch
                      disabled={this.state.switchLoading}
                      value={!this.state.private}
                      onValueChange={async () => {
                        this.setState({switchLoading: true});
                        await this.togglePublic();
                        this.setState({switchLoading: false});
                      }}
                    />
                  </View>
                </View>
              }
            />

            <Gallery
              canDeletePhoto={false}
              visible={this.state.galleryModalVisible}
              galleryVisible={true}
              photos={this.state.photos}
              onRequestClose={() => this.hideGallery()}
              onDeletePhoto={() => {}}
              index={this.state.galleryIndex}
              onPageSelected={(index) => this.setState({galleryIndex: index})}
            />

            <Overlay
              isVisible={this.state.diaryModalVisible}
              onBackdropPress={() => this.setState({diaryModalVisible: false})}
              fullScreen={true}
              borderRadius={0}
              animationType={'slide'}>
              <ScrollView>
                {!this.state.diaryLoading ? (
                  <>
                    <View
                      style={{flex: 1, flexDirection: 'row', marginBottom: 20}}>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          marginRight: 5,
                          marginTop: 5,
                        }}
                        onPress={() => {
                          this.setState({diaryModalVisible: false});
                        }}>
                        <Icon
                          color={variables.darkGreen}
                          size={30}
                          type="material"
                          name="close"
                        />
                      </TouchableOpacity>
                      <View style={{flex: 8}}>
                        <Text h3 style={{color: variables.darkGreen}}>
                          {this.state.diary.title}
                        </Text>
                      </View>
                    </View>

                    <View style={{paddingBottom: 20}}>
                      <Text style={{fontSize: 16, paddingHorizontal: 10}}>
                        {this.state.diary.body}
                      </Text>
                    </View>
                  </>
                ) : (
                  <PageSpinner />
                )}
              </ScrollView>
            </Overlay>

            <Overlay
              isVisible={this.state.shareOverlayVisible}
              onBackdropPress={() =>
                this.setState({shareOverlayVisible: false})
              }>
              <View style={{flex:1}}>
                <View style={{flex:1}}>
                  <Text
                    h4
                    style={{color: variables.lightGreen, marginBottom: 20}}>
                    Your link was copied to clipboard.
                  </Text>
                </View>
                <View style={{flex:2}}>
                  <Text style={{fontSize: 20, marginBottom: 20}}>
                    Share this PIN with the friends you want to show your trip to:
                  </Text>

                  <View
                    style={{
                      marginBottom: 40,
                      height: 60,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.pinLoading ? (
                      <PageSpinner />
                    ) : (
                      <Input
                        disabled={true}
                        inputStyle={{fontSize: 25}}
                        value={this.state.pin}
                      />
                    )}
                  </View>
                </View>

                <View style={{flex:1, justifyContent:'flex-end', marginBottom:20}}>
                  <OutlineButton
                    title="Close"
                    onPress={() => this.setState({shareOverlayVisible: false})}
                  />
                </View>
              </View>
            </Overlay>
          </View>
        )}
      </View>
    );
  }
}
