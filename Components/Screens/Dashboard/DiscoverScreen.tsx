import React, {Component} from 'react';
import {View, Alert, SectionList, Image} from 'react-native';
import {ListItem, Tile, Text} from 'react-native-elements';
import {SearchBar, Card} from 'react-native-elements';
import variables from '../../../StyleSheets/variables';
import {NavigationTabProp} from 'react-navigation-tabs';

import {request, baseUrl} from '../../../helpers';

import Auth from '../../../Modules/Auth';
import PageSpinner from '../../PageSpinner';
import OfflineView from '../../../Components/OfflineView';
import FastImage from 'react-native-fast-image';
import Ad from '../../../Components/Ad';
import {BannerAdSize} from '@react-native-firebase/admob';
import TripThumbnail from '../../../Components/TripThumbnail';

type DiscoverScreenProps = {
  navigation: NavigationTabProp;
};

export default class DiscoverScreen extends Component {
  constructor(props: DiscoverScreenProps) {
    super(props);

    this.onSearch = this.onSearch.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
  }

  state = {
    query: '',
    isLoading: false,
    trips: [],
    users: [],
    typingTimeout: 0,
    noInternet: false,
    password: undefined,
    showSuggestions: true,
    suggestions: [],
    hasAds: true,
  };

  static navigationOptions = {
    header: null,
  };

  async componentDidMount() {
    const user = await Auth.user();
    if (user) {
      this.setState({hasAds: user.has_ads});
    }

    const credentials = await Auth.getCredentials();

    if (!credentials) {
      this.setState({password: undefined});
      return;
    }

    this.setState({password: credentials.password, isLoading: true});

    const response = await request(
      'trip/suggestions',
      'get',
      undefined,
      credentials.password,
      false,
    );

    if (!response) {
      this.setState({isLoading: false});
      return;
    }

    switch (response.status) {
      case 200:
        // all ok
        let responseJson = await response.json();

        this.setState({
          suggestions: responseJson.trips,
        });

        break;
    }

    this.setState({isLoading: false});
  }

  async onSearch(query: string) {
    this.setState({query, isLoading: true});

    query = query.trim();

    if (!query || query.length == 0) {
      this.setState({trips: [], users: [], isLoading: false});

      if (this.state.typingTimeout) {
        clearTimeout(this.state.typingTimeout);
      }

      return;
    }

    if (this.state.typingTimeout) {
      clearTimeout(this.state.typingTimeout);
    }

    this.setState({
      typingTimeout: setTimeout(async () => {
        if (!query || query.length == 0) {
          this.setState({trips: [], users: [], isLoading: false});

          if (this.state.typingTimeout) {
            clearTimeout(this.state.typingTimeout);
          }

          return;
        }

        const response = await request(
          'search/?q=' + query,
          'get',
          undefined,
          this.state.password,
          false,
        );

        if (!response) {
          this.setState({isLoading: false, noInternet: true});
          return;
        }

        this.setState({noInternet: false});

        switch (response.status) {
          case 200:
            // all ok
            let responseJson = await response.json();

            this.setState({
              trips: responseJson.trips,
              users: responseJson.users,
            });

            break;
          default:
            Alert.alert(
              'Error',
              'An unexpected error ocurred. Please try again later.',
              [{text: 'OK', onPress: () => {}}],
              {cancelable: false},
            );
        }

        this.setState({isLoading: false});
      }, 500),
    });
  }

  private renderSectionHeader({section: {title, data}}: any) {
    if (!data || data.length == 0) {
      return null;
    }

    if (title == 'Trips') {
      return null;
    }

    return (
      <View style={{alignItems: 'center', marginVertical: 15}}>
        <Text style={{fontSize: 20}}>{title}</Text>
      </View>
    );
  }

  private renderItem(itemObj: any) {
    const item = itemObj.item;
    const section = itemObj.section;
    const index = itemObj.index;

    if (section.title == 'Users') {
      return (
        <ListItem
          leftAvatar={
            <FastImage
              style={{
                borderRadius: 100,
                width: 45,
                height: 45,
              }}
              source={{
                uri: item.profile_picture_permalink,
              }}
            />
          }
          title={item.name}
          bottomDivider
          chevron
          onPress={() => {
            this.props.navigation.navigate({
              routeName: 'ViewUniversalProfile',
              params: {userId: item.id},
            });
          }}
        />
      );
    }

    const numColumns = 2;

    if (index % numColumns !== 0) return null;

    const items = [];

    for (let i = index; i < index + numColumns; i++) {
      if (i >= section.data.length) {
        break;
      }

      const item = section.data[i];

      let element;

      if (!item.thumbnail_id) {
        element = (
          <TripThumbnail
            hasThumbnail={false}
            onPress={() => {
              this.props.navigation.navigate({
                routeName: 'ViewUniversalTrip',
                params: {tripId: item.id},
              });
            }}
            item={item}
          />
        );
      } else {
        element = (
          <TripThumbnail
            hasThumbnail={true}
            onPress={() => {
              this.props.navigation.navigate({
                routeName: 'ViewUniversalTrip',
                params: {tripId: item.id},
              });
            }}
            source={{
              uri: baseUrl() + 'photo/showThumbnail/' + item.thumbnail_id,
              method: 'get',
              headers: {
                Authorization: 'Bearer ' + this.state.password,
              },
            }}
            item={item}
          />
        );
      }

      items.push(element);
    }

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
        }}>
        {items}
      </View>
    );
  }

  render() {
    return (
      <>
        <View style={{flex: 1, backgroundColor: variables.light}}>
          <SearchBar
            lightTheme
            containerStyle={{
              borderTopWidth: 0,
              backgroundColor: variables.light,
            }}
            inputContainerStyle={{backgroundColor: '#e8e8e8'}}
            placeholder="Search for people, places..."
            onChangeText={this.onSearch}
            value={this.state.query}
            onFocus={() => this.setState({showSuggestions: false})}
            onBlur={() => {
              !this.state.query || !this.state.query.length
                ? this.setState({showSuggestions: true})
                : this.setState({showSuggestions: false});
            }}
          />

          {this.state.noInternet ? (
            <View style={{marginTop: 15}}>
              <OfflineView
                title="No internet"
                onPress={() => {
                  this.onSearch(this.state.query);
                }}
              />
            </View>
          ) : this.state.isLoading ? (
            <View style={{marginTop: 15}}>
              <PageSpinner />
            </View>
          ) : !this.state.isLoading &&
            this.state.query &&
            !this.state.trips.length &&
            !this.state.users.length ? (
            <ListItem title="Nothing matches your search" />
          ) : (
            <SectionList
              keyboardShouldPersistTaps="handled"
              removeClippedSubviews={true}
              sections={[
                {
                  title: 'Trips',
                  data: this.state.showSuggestions
                    ? this.state.suggestions
                    : this.state.trips,
                },
                {title: 'Users', data: this.state.users},
              ]}
              renderSectionHeader={this.renderSectionHeader}
              ListHeaderComponent={
                this.state.showSuggestions ? (
                  <Text style={{marginLeft: 20}} h2>
                    Suggestions
                  </Text>
                ) : null
              }
              renderItem={this.renderItem}
            />
          )}
        </View>

        <Ad
          hasAds={this.state.hasAds}
          id="ca-app-pub-3943805809685999/9815321552"
          size={BannerAdSize.SMART_BANNER}
        />
      </>
    );
  }
}
