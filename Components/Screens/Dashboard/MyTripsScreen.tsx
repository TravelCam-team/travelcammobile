import React, {Component} from 'react';
import {View} from 'react-native';
import {Icon, ListItem} from 'react-native-elements';

import {NavigationTabProp} from 'react-navigation-tabs';
import variables from '../../../StyleSheets/variables';

import {NavigationEventSubscription} from 'react-navigation';

import PageSpinner from '../../../Components/PageSpinner';
import TripListItem from '../../../Components/TripListItem';

import Auth from '../../../Modules/Auth';
import {FlatList} from 'react-native-gesture-handler';

import Ad from '../../../Components/Ad';
import {BannerAdSize} from '@react-native-firebase/admob';

type MyTripsScreenProps = {
  navigation: NavigationTabProp;
};

export default class MyTripsScreen extends Component<MyTripsScreenProps> {
  state = {
    user: undefined,
    isLoading: false,
  };

  constructor(props: MyTripsScreenProps) {
    super(props);

    this.renderItem = this.renderItem.bind(this);
  }

  static navigationOptions = {
    header: null,
  };

  private willFocusListener: NavigationEventSubscription | null = null;

  async componentDidMount() {
    this.willFocusListener = this.props.navigation.addListener(
      'willFocus',
      async () => {
        await this.performRefresh();
      },
    );

    await this.performRefresh();
  }

  componentWillUnmount() {
    this.willFocusListener!.remove();
  }

  async performRefresh() {
    this.setState({isLoading: true});
    const user = await Auth.user();

    this.setState({user, isLoading: false});
  }

  private renderItem(itemObj: any) {
    const item = itemObj.item;

    return (
      <TripListItem
        item={item}
        onPress={() => {
          this.props.navigation.navigate({
            routeName: 'ViewTrip',
            params: {tripId: item.id},
          });
        }}
      />
    );
  }

  render() {
    let content;

    if (this.state.isLoading) {
      content = <PageSpinner />;
    } else {
      content = this.state.user ? (
        <FlatList
          removeClippedSubviews={true}
          data={this.state.user.ended_trips}
          renderItem={this.renderItem}
          keyExtractor={(item) => item.ended_at}
          ListEmptyComponent={<ListItem title="No finished trips yet" />}
        />
      ) : null;
    }

    return (
      <>
        <View style={{flex: 1, backgroundColor: variables.light}}>
          {content}
        </View>
        {this.state.user ? (
          <Ad
            hasAds={this.state.user.has_ads}
            id="ca-app-pub-3943805809685999/1472074906"
            size={BannerAdSize.SMART_BANNER}
          />
        ) : null}
      </>
    );
  }
}
