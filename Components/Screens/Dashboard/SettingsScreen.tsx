import React, {Component} from 'react';
import {View} from 'react-native';
import {Icon, Button} from 'react-native-elements';

import {NavigationTabProp} from 'react-navigation-tabs';
import variables from '../../../StyleSheets/variables';
import styles from '../../../StyleSheets/Screens/Dashboard/SettingsScreen';
import {showMessage, openUrl} from '../../../helpers';

import OutlineButton from '../../../Components/OutlineButton';
import PageSpinner from '../../../Components/PageSpinner';

import Ad from '../../../Components/Ad';
import {BannerAdSize} from '@react-native-firebase/admob';

import Auth from '../../../Modules/Auth';

type SettingsScreenProps = {
  navigation: NavigationTabProp;
};

export default class SettingsScreen extends Component<SettingsScreenProps> {
  state = {
    isLoading: true,
    hasAds: true,
  };

  private async logOut() {
    this.setState({isLoading: true});

    await Auth.logOut();

    showMessage('Logged out');

    this.props.navigation.navigate('Landing');
  }

  async componentDidMount() {
    const user = await Auth.user();

    this.setState({isLoading: false, hasAds: user.has_ads});
  }

  render() {
    let content;

    if (this.state.isLoading) {
      content = <PageSpinner />;
    }

    return (
      <>
        <View style={styles.mainView}>
          {content}

          {!this.state.isLoading && (
            <>
              <OutlineButton
                title="Edit description"
                icon={
                  <Icon
                    name="account-edit"
                    type="material-community"
                    color={variables.lightGreen}
                    size={variables.inputIconSize}
                  />
                }
                onPress={() =>
                  this.props.navigation.navigate('EditDescription')
                }
              />

              <View style={{marginTop: 10}}>
                <OutlineButton
                  title="Contact us"
                  icon={
                    <Icon
                      name="contact-mail"
                      type="material-community"
                      color={variables.lightGreen}
                      size={variables.inputIconSize}
                    />
                  }
                  onPress={() =>
                    openUrl(
                      'mailto:travelcam.app@gmail.com',
                      'Email us at travelcam.app@gmail.com',
                    )
                  }
                />
              </View>

              <Button
                type="clear"
                title="Log out"
                titleStyle={{
                  marginLeft: 10,
                  color: variables.lightGreen,
                }}
                containerStyle={{marginTop: 10}}
                icon={
                  <Icon
                    name="logout"
                    type="material-community"
                    color={variables.lightGreen}
                    size={variables.inputIconSize}
                  />
                }
                onPress={() => this.logOut()}
              />
            </>
          )}
        </View>
        <Ad
          hasAds={this.state.hasAds}
          id="ca-app-pub-3943805809685999/1179365209"
          size={BannerAdSize.SMART_BANNER}
        />
      </>
    );
  }
}
