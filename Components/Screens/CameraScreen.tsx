import React, {Component} from 'react';

import {RNCamera} from 'react-native-camera';

import {
  View,
  TouchableOpacity,
  Text,
  TouchableWithoutFeedback,
  Dimensions,
  Platform,
  StatusBar,
  Alert,
} from 'react-native';
import {NavigationTabProp} from 'react-navigation-tabs';
import {NavigationEventSubscription} from 'react-navigation';

import {Icon} from 'react-native-elements';

import FastImage from 'react-native-fast-image';

import {
  setTranslucentStatusBar,
  setWhiteStatusBar,
  now,
  deleteFileIfExists,
} from '../../helpers';

import ZoomView from '../ZoomView';
import styles from '../../StyleSheets/Screens/CameraScreen';
import Auth from '../../Modules/Auth';
import PhotoManager from '../../Modules/PhotoManager';

import Gallery from '../Gallery';

import variables from '../../StyleSheets/variables';

import {State, TapGestureHandler} from 'react-native-gesture-handler';

type CameraScreenProps = {
  navigation: NavigationTabProp;
};

const MAX_ZOOM = 7; // iOS only
const ZOOM_F = Platform.OS === 'ios' ? 0.007 : 0.08;

export default class CameraScreen extends Component<CameraScreenProps> {
  static navigationOptions = {
    tabBarVisible: false,
  };

  state = {
    activeTrip: false,
    flash: 2,
    frontCamera: false,
    cameraCount: 1,
    takingPicture: false,
    zoom: 0.0,
    focusPoint: undefined,
    focusPointCircle: undefined,
    lastPhotoUri: undefined,
    galleryVisible: false,
    galleryModalVisible: false,
    offlinePhotos: [],
    galleryIndex: 0,
  };

  private prevPinch: number = 1;

  // ref to camera element
  private camera: any;

  private focusListener: NavigationEventSubscription | null = null;
  private willFocusListener: NavigationEventSubscription | null = null;
  private blurListener: NavigationEventSubscription | null = null;

  onPinchStart = () => {
    this.prevPinch = 1;
  };

  onPinchEnd = () => {
    this.prevPinch = 1;
  };

  onPinchProgress = (p: number) => {
    let p2 = p - this.prevPinch;
    if (p2 > 0 && p2 > ZOOM_F) {
      this.prevPinch = p;
      this.setState({zoom: Math.min(this.state.zoom + ZOOM_F, 1)}, () => {});
    } else if (p2 < 0 && p2 < -ZOOM_F) {
      this.prevPinch = p;
      this.setState({zoom: Math.max(this.state.zoom - ZOOM_F, 0)}, () => {});
    }
  };

  async componentDidMount() {
    await this.reloadActiveTrip();
    await this.reloadLastPhoto();

    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      setTranslucentStatusBar();
    });

    this.blurListener = this.props.navigation.addListener('didBlur', () => {
      setWhiteStatusBar();
    });

    this.willFocusListener = this.props.navigation.addListener(
      'willFocus',
      async () => {
        await this.reloadActiveTrip();
        await this.reloadLastPhoto();
      },
    );

    await this.getOfflinePhotos();
  }

  componentWillUnmount() {
    this.focusListener!.remove();
    this.blurListener!.remove();
    this.willFocusListener!.remove();
  }

  private async reloadLastPhoto() {
    const uri = await PhotoManager.getLastPhotoUri();

    this.setState({lastPhotoUri: uri});
  }

  private async reloadActiveTrip() {
    const user = await Auth.user();

    if (user && user.has_active_trip && this.state.activeTrip === false) {
      this.setState({activeTrip: true});
    } else if (
      user &&
      !user.has_active_trip &&
      this.state.activeTrip === true
    ) {
      this.setState({activeTrip: false});
    }
  }

  private switchCamera() {
    this.setState({
      frontCamera: !this.state.frontCamera,
      zoom: 0.0,
    });
  }

  private onSingleTap(event: any) {
    if (event.nativeEvent.state === State.ACTIVE) {
      const {absoluteX, absoluteY} = event.nativeEvent;
      const x0 = absoluteX / Dimensions.get('screen').width;
      const y0 = absoluteY / Dimensions.get('screen').height;

      const x = y0;
      const y = -x0 + 1;

      // briefly show focus circle
      this.setState({
        focusPoint: {
          x: x,
          y: y,
        },
        focusPointCircle: [absoluteX, absoluteY],
      });

      setTimeout(() => this.setState({focusPointCircle: undefined}), 150);
    }
  }

  private async getOfflinePhotos() {
    const photos = await PhotoManager.getLatestOfflinePhotos();

    if (photos) {
      this.setState({
        offlinePhotos: photos,
      });
    } else {
      this.setState({
        offlinePhotos: [],
      });
    }
  }

  private async hasExceededAllowedPhotos() {
    const nrPhotos = this.state.offlinePhotos.length;
    const userData = await Auth.user();
    const allowedPhotos = userData.allowed_photos_per_trip;

    if (nrPhotos >= allowedPhotos) {
      return true;
    }

    return false;
  }

  private async takePicture() {
    if (this.state.takingPicture) {
      return;
    }

    if (await this.hasExceededAllowedPhotos()) {
      Alert.alert(
        'Error',
        'Exceeded maximum number of photos per trip.',
        [{text: 'OK', onPress: () => {}}],
        {cancelable: false},
      );

      return;
    }

    this.setState({takingPicture: true});

    if (this.camera) {
      const options = {
        quality: 0.5,
        base64: false,
        orientation: RNCamera.Constants.Orientation.portrait,
        fixOrientation: true,
      };

      const data = await this.camera.takePictureAsync(options);

      this.setState({lastPhotoUri: data.uri});

      await PhotoManager.addOfflinePhotos([{uri: data.uri, created_at: now()}]);

      await this.getOfflinePhotos();
    }
  }

  private async deletePhoto() {
    const index = this.state.galleryIndex;
    const tempPhoto = this.state.offlinePhotos[index];

    this.setState({galleryVisible: false});
    await PhotoManager.removeOfflinePhoto(tempPhoto.uri);
    await this.reloadLastPhoto();

    const filePath = tempPhoto.uri.split('///').pop();

    deleteFileIfExists(filePath);

    let temp = this.state.offlinePhotos;
    temp.splice(index, 1);
    this.setState({offlinePhotos: temp});

    if (this.state.offlinePhotos.length) {
      setTimeout(async () => await this.showGallery(), 1);
    } else {
      this.setState({galleryModalVisible: false});
      StatusBar.setHidden(false);
    }
  }

  private async showGallery() {
    StatusBar.setHidden(true);

    await this.getOfflinePhotos();

    this.setState({galleryVisible: true, galleryModalVisible: true});
  }

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          autoFocus="on"
          autoFocusPointOfInterest={
            this.state.focusPoint ? this.state.focusPoint : undefined
          }
          style={styles.preview}
          type={
            this.state.frontCamera
              ? RNCamera.Constants.Type.front
              : RNCamera.Constants.Type.back
          }
          flashMode={
            this.state.flash == 0
              ? RNCamera.Constants.FlashMode.on
              : this.state.flash == 1
              ? RNCamera.Constants.FlashMode.off
              : RNCamera.Constants.FlashMode.auto
          }
          captureAudio={false}
          onCameraReady={async () =>
            this.setState({
              cameraCount: (await this.camera.getCameraIdsAsync()).length,
            })
          }
          // unidentifiable error with react camera type declaration
          onPictureTaken={() => this.setState({takingPicture: false})}
          zoom={this.state.zoom}
          maxZoom={MAX_ZOOM}>
          <ZoomView
            onPinchEnd={this.onPinchEnd}
            onPinchStart={this.onPinchStart}
            onPinchProgress={this.onPinchProgress}>
            <TapGestureHandler
              numberOfTaps={1}
              onHandlerStateChange={event => this.onSingleTap(event)}>
              <TapGestureHandler
                onHandlerStateChange={event => {
                  if (event.nativeEvent.state === State.ACTIVE) {
                    this.switchCamera();
                  }
                }}
                numberOfTaps={2}>
                <View
                  style={{
                    height: Dimensions.get('window').height,
                    width: '100%',
                  }}></View>
              </TapGestureHandler>
            </TapGestureHandler>
          </ZoomView>
        </RNCamera>

        {!this.state.activeTrip && (
          <TouchableWithoutFeedback
            onPress={() => {
              this.props.navigation.navigate('NewTrip');
            }}>
            <View style={styles.noCameraView}>
              <View style={styles.noCameraViewHeading}>
                <Icon
                  color="white"
                  name="close"
                  type="material-community"
                  size={27}
                />
                <Text style={styles.noCameraViewHeadingText}>
                  No active trip
                </Text>
              </View>
              <Text style={styles.noCameraViewText}>
                You can only use the camera during trips.
              </Text>
            </View>
          </TouchableWithoutFeedback>
        )}

        <View style={styles.controlsTop}>
          <TouchableOpacity
            style={styles.flashView}
            onPress={() => {
              this.setState({flash: (this.state.flash + 1) % 3});
            }}>
            <Icon
              name={
                this.state.flash == 0
                  ? 'flash'
                  : this.state.flash == 1
                  ? 'flash-off'
                  : 'flash-auto'
              }
              type="material-community"
              color="white"
              size={35}
            />
          </TouchableOpacity>

          {this.state.cameraCount == 2 ? (
            <TouchableOpacity
              style={styles.switchCameraView}
              onPress={() => this.switchCamera()}>
              <Icon
                name="switch-camera"
                type="material"
                color="white"
                size={35}
              />
            </TouchableOpacity>
          ) : null}
        </View>

        <View style={styles.controls}>
          {this.state.activeTrip && (
            <TouchableOpacity
              onPress={this.takePicture.bind(this)}
              style={[
                styles.capture,
                {
                  backgroundColor: this.state.takingPicture
                    ? variables.lightGreen
                    : 'transparent',
                  borderColor: this.state.takingPicture
                    ? 'transparent'
                    : 'white',
                },
              ]}></TouchableOpacity>
          )}

          {this.state.activeTrip && this.state.lastPhotoUri && (
            <>
              <View
                style={{
                  flex: 1,
                  position: 'absolute',
                  left: 20,
                  bottom: 50,
                  borderColor: 'white',
                  borderWidth: 3,
                }}>
                <TouchableWithoutFeedback
                  onPress={async () => {
                    this.setState({galleryIndex: 0});
                    await this.showGallery();
                  }}>
                  <FastImage
                    style={{width: 50, height: 50}}
                    source={{uri: this.state.lastPhotoUri}}
                  />
                </TouchableWithoutFeedback>
              </View>

              <Gallery
                canDeletePhoto={true}
                visible={this.state.galleryModalVisible}
                galleryVisible={this.state.galleryVisible}
                photos={this.state.offlinePhotos}
                onRequestClose={() => {
                  this.setState({
                    galleryIndex: 0,
                    galleryVisible: false,
                    galleryModalVisible: false,
                  });
                  StatusBar.setHidden(false);
                }}
                index={this.state.galleryIndex}
                onDeletePhoto={async () => await this.deletePhoto()}
                onPageSelected={index => this.setState({galleryIndex: index})}
              />
            </>
          )}
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('Home');
            }}
            style={styles.back}>
            <Icon
              name="chevron-right"
              type="material-community"
              color="white"
              size={50}
            />
          </TouchableOpacity>
        </View>

        {this.state.focusPointCircle &&
        this.state.focusPointCircle.length == 2 ? (
          <View
            style={{
              height: 50,
              width: 50,
              borderRadius: 100,
              backgroundColor: 'transparent',
              borderColor: 'white',
              borderWidth: 1,
              position: 'absolute',
              left: this.state.focusPointCircle[0],
              top: this.state.focusPointCircle[1],
            }}></View>
        ) : null}
      </View>
    );
  }
}
