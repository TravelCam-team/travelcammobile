import React, {Component} from 'react';
import {ScrollView, View, Alert} from 'react-native';
import {Icon, Input, Button} from 'react-native-elements';
import variables from '../../StyleSheets/variables';
import {NavigationStackProp} from 'react-navigation-stack';

import styles from '../../StyleSheets/Screens/NewDiaryScreen';

import {request, showMessage, now} from '../../helpers';
import DiaryManager from '../../Modules/DiaryManager';
import StackHeader from '../StackHeader';

type NewDiaryScreenProps = {
  navigation: NavigationStackProp;
};

export default class NewDiaryScreen extends Component<NewDiaryScreenProps> {
  static navigationOptions = ({navigation}) => {
    return {
      tabBarVisible: false,
      headerTitle: () => <StackHeader title="New Diary" />,
      headerBackImage: (param: {
        tintColor?: string | undefined;
        title?: string | null | undefined;
      }) => {
        return (
          <Icon
            color={param.tintColor}
            name="close"
            type="material-community"
            size={27}
          />
        );
      },
      headerRight: () => (
        <Button
          buttonStyle={{backgroundColor: 'transparent', marginRight: 10}}
          onPress={async () => {
            const func = navigation.getParam('onSubmit');
            await func();
          }}
          icon={
            <Icon
              color={variables.darkGreen}
              name="check"
              type="material-community"
              size={27}
            />
          }
        />
      ),
    };
  };

  state = {
    isLoading: false,
    title: '',
    body: '',
    errors: {title: [], body: []},
  };

  private bodyInput: any;

  componentDidMount() {
    this.props.navigation.setParams({onSubmit: this.onSubmit});
  }

  componentWillUnmount() {
    this.props.navigation.setParams({onSubmit: undefined});
  }

  onSubmit = async () => {
    this.setState({
      errors: {
        title: [''],
        body: [''],
      },
      isLoading: true,
    });

    const errors = this.getValidationErrors();

    this.setState({
      title: this.state.title.trim(),
      body: this.state.body.trim(),
    });

    if (errors) {
      this.setState({
        errors: errors,
        isLoading: false,
      });
      return;
    }

    await DiaryManager.addOfflineDiary({
      title: this.state.title.trim(),
      body: this.state.body.trim(),
      created_at: now(),
    });

    this.setState({
      isLoading: false,
    });

    this.props.navigation.navigate('Home');
  };

  private getValidationErrors() {
    let errors = {title: [], body: []},
      hasErrors = false;

    const title = this.state.title.trim();
    const body = this.state.body.trim();

    if (!title || !title.length) {
      errors.title.push('The title field is required');
      hasErrors = true;
    }

    if (title.length > 100) {
      errors.title.push('The title can have a maximum length of 100');
      hasErrors = true;
    }

    if (!body || !body.length) {
      errors.body.push('The body field is required');
      hasErrors = true;
    }

    if (hasErrors) {
      return errors;
    }

    return null;
  }

  render() {
    return (
      <View style={styles.mainView}>
        <ScrollView
          style={styles.scrollView}
          keyboardShouldPersistTaps="handled">
          <View style={[styles.inputView]}>
            <Input
              placeholder="First day in Hawaii..."
              label="Title"
              value={this.state.title}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors['title'] ? this.state.errors['title'][0] : ''
              }
              onChangeText={title => this.setState({title})}
              returnKeyType="next"
              onSubmitEditing={() => {
                this.bodyInput.focus();
              }}
              blurOnSubmit={false}
              autoFocus={true}
            />
          </View>

          <View style={[styles.inputView]}>
            <Input
              ref={bodyInput => {
                this.bodyInput = bodyInput;
              }}
              placeholder="Dear diary..."
              label="Diary"
              value={this.state.body}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors['body'] ? this.state.errors['body'][0] : ''
              }
              onChangeText={body => this.setState({body})}
              returnKeyType="none"
              multiline={true}
              inputContainerStyle={{
                borderBottomWidth: 0,
              }}
              inputStyle={{textAlignVertical: 'top'}}
              numberOfLines={10}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
