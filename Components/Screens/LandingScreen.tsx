import React, {Component} from 'react';
import {View, ActivityIndicator, Alert} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import Auth from '../../Modules/Auth';

import WhiteStatusBar from '../WhiteStatusBar';
import {request, showMessage, setTranslucentStatusBar} from '../../helpers';
import styles from '../../StyleSheets/Screens/LandingScreen';

import {NavigationStackProp} from 'react-navigation-stack';
import variables from '../../StyleSheets/variables';

type LandingScreenProps = {
  navigation: NavigationStackProp;
};

export default class LandingScreen extends Component<LandingScreenProps> {
  componentDidMount() {
    this.navigate();
  }

  private async guest() {
    const route = this.props.navigation.getParam('route');

    if (!route) {
      return false;
    }

    // if the password was reset, output a message
    if (route == 'PasswordReset') {
      Alert.alert(
        'Info',
        'Your password was reset. You may now Log In',
        [
          {
            text: 'Ok',
            onPress: () => {},
          },
        ],
        {
          cancelable: false,
        },
      );

      this.props.navigation.navigate('LogIn');

      return true;
    }

    // if the email was activated, attempt a login
    if (route == 'EmailVerified') {
      const id = this.props.navigation.getParam('user_id');
      const token = this.props.navigation.getParam('user_token');

      if (!id || !token) {
        return false;
      }

      const response = await request(
        'user/authorize',
        'get',
        undefined,
        id + '.' + token,
      );

      if (!response) {
        return;
      }

      switch (response.status) {
        case 200:
          // all OK, authenticate user
          const responseJson = await response.json();

          await Auth.logIn(id, token, responseJson);

          showMessage('Logged in');
          this.props.navigation.navigate('App');
          return true;
        default:
          // invalid token
          return false;
      }
    }
  }

  private redirectToCamera() {
    setTranslucentStatusBar();
    this.props.navigation.navigate('Camera');
  }

  // Navigate to the appropriate screen
  private async navigate() {
    const credentials = await Auth.getCredentials();

    SplashScreen.hide();

    if (credentials) {
      const response = await request(
        'user/authorize',
        'get',
        undefined,
        credentials.password,
        false,
      );

      if (!response) {
        // authenticated, but user is offline
        const userData = await Auth.user();

        if (userData && userData.has_active_trip) {
          this.redirectToCamera();
          return;
        }

        this.props.navigation.navigate('Home');
        return;
      }

      switch (response.status) {
        case 200:
          // all OK, user is authenticated

          const userData = await response.json();
          await Auth.setUserData(userData);

          if (userData && userData.has_active_trip) {
            this.redirectToCamera();
            return;
          }

          this.props.navigation.navigate('Home');
          return;
        case 403:
          // all OK, user is not authenticated

          const redirected = await this.guest();

          if (!redirected) {
            this.props.navigation.navigate('Auth');
          }
          return;
        default:
          await this.tryAgain();
      }
    } else {
      const redirected = await this.guest();

      if (!redirected) {
        this.props.navigation.navigate('Auth');
      }
    }
  }

  private async tryAgain() {
    Alert.alert(
      'Error',
      'Authentication error. Please log in again.',
      [{text: 'OK', onPress: () => {}}],
      {cancelable: false},
    );

    await Auth.logOut();

    this.props.navigation.navigate('LogIn');
  }

  // renders loading page
  render() {
    return (
      <View style={styles.loadingView}>
        <WhiteStatusBar />
        <ActivityIndicator
          size={variables.landingSpinnerSize}
          color={variables.darkGreen}
        />
      </View>
    );
  }
}
