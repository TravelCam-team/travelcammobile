import React, {Component} from 'react';
import {View, Alert, ActivityIndicator} from 'react-native';

import styles from '../../StyleSheets/Screens/EndTripLoadingScreen';
import {NavigationStackProp} from 'react-navigation-stack';
import variables from '../../StyleSheets/variables';

import {request, showMessage, baseUrl, deleteFileIfExists} from '../../helpers';
import Auth from '../../Modules/Auth';
import PhotoManager from '../../Modules/PhotoManager';
import DiaryManager from '../../Modules/DiaryManager';

import WhiteStatusBar from '../../Components/WhiteStatusBar';

type DeleteTripLoadingScreenProps = {
  navigation: NavigationStackProp;
};

export default class DeleteTripLoadingScreen extends Component<
  DeleteTripLoadingScreenProps
> {
  state = {
    password: undefined,
  };

  async componentDidMount() {
    const user = await Auth.getCredentials();

    if (user) {
      this.setState({password: user.password});

      await this.initDeleteTrip();
    }
  }

  private async checkInternetConnection() {
    const response = await request(
      'user/authorize',
      'get',
      undefined,
      undefined,
    );

    if (!response) {
      return false;
    }

    return true;
  }

  private async initDeleteTrip() {
    const success = await this.checkInternetConnection();

    if (success) {
      await this.deletePhotos();

      // remove offline photos and diaries from AsyncStorage
      await PhotoManager.removeOfflinePhotos();
      await DiaryManager.removeOfflineDiaries();

      await this.deleteTrip();
    }

    this.props.navigation.navigate('Home');
  }

  private async deletePhotos() {
    const photos = await PhotoManager.getOfflinePhotos();

    if (photos) {
      for (let photo of photos) {
        const filePath = photo.uri.split('///').pop();

        // delete image from device
        if (filePath) {
          deleteFileIfExists(filePath);
        }
      }
    }
  }

  private async deleteTrip() {
    const userData = await Auth.user();
    const response = await request(
      'trip/' + userData.has_active_trip.id + '/delete',
      'post',
      undefined,
      this.state.password,
    );

    if (!response) {
      return;
    }

    let responseJson;
    switch (response.status) {
      case 200:
        // all ok, update saved userData
        responseJson = await response.json();
        await Auth.setUserData(responseJson.user);
        showMessage(responseJson.message);

        break;
      case 403:
        // Output messsage from server
        responseJson = await response.json();
        Alert.alert(
          'Error',
          responseJson.message,
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
        break;
      default:
        Alert.alert(
          'Error',
          'Unexpected error ocurred',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }
  }

  render() {
    return (
      <View style={styles.loadingView}>
        <WhiteStatusBar />
        <ActivityIndicator
          size={variables.landingSpinnerSize}
          color={variables.darkGreen}
        />
      </View>
    );
  }
}
