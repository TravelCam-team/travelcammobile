import React, {Component} from 'react';
import {View, Alert, Text} from 'react-native';

import styles from '../../StyleSheets/Screens/EndTripLoadingScreen';
import {NavigationStackProp} from 'react-navigation-stack';
import variables from '../../StyleSheets/variables';

import {request, showMessage, baseUrl, deleteFileIfExists} from '../../helpers';
import Auth from '../../Modules/Auth';
import PhotoManager from '../../Modules/PhotoManager';
import DiaryManager from '../../Modules/DiaryManager';

import WhiteStatusBar from '../../Components/WhiteStatusBar';

import ProgressBar from 'react-native-progress/Bar';

import Ad from '../../Components/Ad';
import {BannerAdSize} from '@react-native-firebase/admob';

type EndTripLoadingScreenProps = {
  navigation: NavigationStackProp;
};

export default class EndTripLoadingScreen extends Component<
  EndTripLoadingScreenProps
> {
  state = {
    password: undefined,
    progress: 0,
    status: 'Initializing...',
    hasAds: true,
  };

  async componentDidMount() {
    const user = await Auth.getCredentials();
    const userData = await Auth.user();

    if (userData) {
      this.setState({hasAds: userData.has_ads});
    }

    if (user) {
      this.setState({password: user.password});

      await this.initEndTrip();
    }
  }

  private async checkInternetConnection() {
    const response = await request(
      'user/authorize',
      'get',
      undefined,
      undefined,
    );

    if (!response) {
      return false;
    }

    return true;
  }

  private async initEndTrip() {
    const success = await this.checkInternetConnection();

    if (success) {
      this.setState({status: 'Uploading photos'});
      await this.uploadPhotos();
      this.setState({progress: 0.33});

      this.setState({status: 'Uploading diaries'});
      await this.uploadDiaries();
      this.setState({progress: 0.66});

      this.setState({status: 'Clearing data from device'});
      // remove offline photos and diaries from AsyncStorage
      await PhotoManager.removeOfflinePhotos();
      await DiaryManager.removeOfflineDiaries();
      this.setState({progress: 0.8});

      this.setState({status: 'Ending the trip'});
      await this.endTrip();

      this.setState({progress: 1});
    }

    this.props.navigation.navigate('MyTrips');
  }

  private async endTrip() {
    const response = await request(
      'user/endActiveTrip',
      'post',
      undefined,
      this.state.password,
    );

    if (!response) {
      return;
    }

    let responseJson;
    switch (response.status) {
      case 200:
        // all ok, update saved userData and refresh page now
        responseJson = await response.json();
        await Auth.setUserData(responseJson.user);
        showMessage('Trip ended');

        break;
      case 403:
        // Output messsage from server
        responseJson = await response.json();
        Alert.alert(
          'Error',
          responseJson.message,
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
        break;
      default:
        Alert.alert(
          'Error',
          'Unexpected error ocurred',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }
  }

  private async uploadPhotos() {
    const photos = await PhotoManager.getOfflinePhotos();

    if (photos) {
      const increment = 0.33 / photos.length;

      for (let photo of photos) {
        await this.uploadPhoto(photo);

        this.setState({progress: this.state.progress + increment});
      }
    }
  }

  private async uploadPhoto(data: any) {
    let body = new FormData();
    body.append('photo', {
      uri: data.uri,
      name: data.uri,
      type: 'image/jpg',
    });

    body.append('taken_at', data.created_at);

    let user = await Auth.user();
    const trip = user.has_active_trip.id;
    user = await Auth.getCredentials();

    const response = await fetch(baseUrl() + 'photo/' + trip + '/new', {
      method: 'post',
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + user.password,
      },
      body: body,
    });

    let responseJson;

    switch (response.status) {
      case 201:
        // all OK, picture was uploaded
        responseJson = await response.json();

        const filePath = data.uri.split('///').pop();

        // delete image from device
        deleteFileIfExists(filePath);
        break;
      case 422:
        // validation issues
        responseJson = await response.json();

        if (responseJson.errors && responseJson.errors.photo) {
          Alert.alert(
            'Error',
            responseJson.errors.photo[0],
            [{text: 'OK', onPress: () => {}}],
            {cancelable: false},
          );
        } else {
          Alert.alert(
            'Error',
            'There was an error processing your photo.',
            [{text: 'OK', onPress: () => {}}],
            {cancelable: false},
          );
        }
        return;
    }
  }

  private async uploadDiaries() {
    const diaries = await DiaryManager.getOfflineDiaries();

    if (diaries) {
      for (let diary of diaries) {
        this.uploadDiary(diary);
      }
    }
  }

  private async uploadDiary(data: any) {
    let user = await Auth.user();
    const trip = user.has_active_trip.id;
    user = await Auth.getCredentials();

    const response = await request(
      'diary/' + trip + '/new',
      'post',
      {title: data.title, body: data.body, taken_at: data.created_at},
      user.password,
    );

    if (!response) {
      this.setState({
        isLoading: false,
      });

      return;
    }

    switch (response.status) {
      case 201:
        // all OK, diary was uploaded

        break;
      case 422:
        // validation issues

        Alert.alert(
          'Error',
          'There was an error processing your diary.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );

        return;
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={styles.loadingView}>
          <WhiteStatusBar />

          <ProgressBar
            progress={this.state.progress}
            width={200}
            color={variables.darkGreen}
          />

          <Text style={{marginTop: 20, fontSize: 20}}>{this.state.status}</Text>
        </View>
        <View
          style={{
            marginTop: 20,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Ad
            isMediumRectangle={true}
            hasAds={this.state.hasAds}
            id="ca-app-pub-3943805809685999/8636055534"
            size={BannerAdSize.MEDIUM_RECTANGLE}
          />
        </View>
      </View>
    );
  }
}
