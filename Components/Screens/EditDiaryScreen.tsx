import React, {Component} from 'react';
import {ScrollView, View, Text} from 'react-native';
import {Button, Icon, Input} from 'react-native-elements';
import variables from '../../StyleSheets/variables';
import {NavigationStackProp} from 'react-navigation-stack';

import styles from '../../StyleSheets/Screens/EditDiaryScreen';

import StackHeader from '../StackHeader';
import DiaryManager from '../../Modules/DiaryManager';

type EditDiaryScreenProps = {
  navigation: NavigationStackProp;
};

export default class EditDiaryScreen extends Component<EditDiaryScreenProps> {
  static navigationOptions = ({navigation}) => {
    return {
      tabBarVisible: false,
      headerTitle: () => <StackHeader title="Edit Diary" />,
      headerBackImage: (param: {
        tintColor?: string | undefined;
        title?: string | null | undefined;
      }) => {
        return (
          <Icon
            color={param.tintColor}
            name="close"
            type="material-community"
            size={27}
          />
        );
      },
      headerRight: () => (
        <Button
          buttonStyle={{backgroundColor: 'transparent', marginRight: 10}}
          onPress={async () => {
            const func = navigation.getParam('onSubmit');
            await func();
          }}
          icon={
            <Icon
              color={variables.darkGreen}
              name="check"
              type="material-community"
              size={27}
            />
          }
        />
      ),
    };
  };

  state = {
    isLoading: false,
    diary: undefined,
    title: '',
    body: '',
    errors: {title: [], body: []},
  };

  private bodyInput: any;

  async componentDidMount() {
    this.props.navigation.setParams({
      onSubmit: this.onSubmit,
    });

    const diary = await DiaryManager.getOfflineDiary(
      this.props.navigation.getParam('diary'),
    );

    if (!diary) {
      return;
    }

    this.setState({diary: diary, title: diary?.title, body: diary?.body});
  }

  componentWillUnmount() {
    this.setState({diary: undefined});

    this.props.navigation.setParams({
      onSubmit: undefined,
    });
  }

  onSubmit = async () => {
    if (!this.state.diary) {
      return;
    }

    this.setState({
      errors: {
        title: [''],
        body: [''],
      },
      isLoading: true,
    });

    const errors = this.getValidationErrors();

    this.setState({
      title: this.state.title.trim(),
      body: this.state.body.trim(),
    });

    if (errors) {
      this.setState({
        errors: errors,
        isLoading: false,
      });
      return;
    }

    await DiaryManager.updateOfflineDiary(this.state.diary.uuid, {
      title: this.state.title.trim(),
      body: this.state.body.trim(),
      created_at: this.state.diary.created_at,
    });

    this.setState({
      isLoading: false,
    });

    this.props.navigation.navigate({
      routeName: 'ViewDiary',
      params: {diary: this.state.diary.uuid, refresh: true},
    });
  };

  private getValidationErrors() {
    let errors = {title: [], body: []},
      hasErrors = false;

    const title = this.state.title.trim();
    const body = this.state.body.trim();

    if (!title || !title.length) {
      errors.title.push('The title field is required');
      hasErrors = true;
    }

    if (title.length > 100) {
      errors.title.push('The title can have a maximum length of 100');
      hasErrors = true;
    }

    if (!body || !body.length) {
      errors.body.push('The body field is required');
      hasErrors = true;
    }

    if (hasErrors) {
      return errors;
    }

    return null;
  }

  render() {
    if (this.state.diary) {
      return (
        <View style={styles.mainView}>
          <ScrollView
            style={styles.scrollView}
            keyboardShouldPersistTaps="handled">
            <View style={[styles.inputView]}>
              <Input
                placeholder="First day in Hawaii..."
                label="Title"
                value={this.state.title}
                errorStyle={styles.errorStyle}
                errorMessage={
                  this.state.errors['title']
                    ? this.state.errors['title'][0]
                    : ''
                }
                onChangeText={title => this.setState({title})}
                returnKeyType="next"
                onSubmitEditing={() => {
                  this.bodyInput.focus();
                }}
                blurOnSubmit={false}
                autoFocus={true}
              />
            </View>

            <View style={[styles.inputView]}>
              <Input
                ref={bodyInput => {
                  this.bodyInput = bodyInput;
                }}
                placeholder="Dear diary..."
                label="Diary"
                value={this.state.body}
                errorStyle={styles.errorStyle}
                errorMessage={
                  this.state.errors['body'] ? this.state.errors['body'][0] : ''
                }
                onChangeText={body => this.setState({body})}
                returnKeyType="none"
                multiline={true}
                inputContainerStyle={{
                  borderBottomWidth: 0,
                }}
                inputStyle={{textAlignVertical: 'top'}}
                numberOfLines={10}
              />
            </View>
          </ScrollView>
        </View>
      );
    }

    return <></>;
  }
}
