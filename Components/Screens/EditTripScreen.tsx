import React, {Component} from 'react';
import {ScrollView, View, Alert} from 'react-native';
import {Icon, Input, Button} from 'react-native-elements';
import variables from '../../StyleSheets/variables';
import {NavigationStackProp} from 'react-navigation-stack';

import StackHeader from '../StackHeader';
import OutlineButton from '../OutlineButton';
import styles from '../../StyleSheets/Screens/EditTripScreen';

import Auth from '../../Modules/Auth';
import {request, showMessage} from '../../helpers';

type EditTripScreenProps = {
  navigation: NavigationStackProp;
};

export default class EditTripScreen extends Component<EditTripScreenProps> {
  static navigationOptions = {
    tabBarVisible: false,
    headerTitle: () => <StackHeader title="Edit Trip" />,
    headerBackImage: (param: {
      tintColor?: string | undefined;
      title?: string | null | undefined;
    }) => {
      return (
        <Icon
          color={param.tintColor}
          name="close"
          type="material-community"
          size={27}
        />
      );
    },
  };

  state = {
    isLoading: false,
    name: '',
    location: '',
    errors: {'trip.name': [], 'trip.location': []},
  };

  async componentDidMount() {
    const user = await Auth.user();

    if (user && user.has_active_trip) {
      return this.setState({
        name: user.has_active_trip.name,
        location: user.has_active_trip.location,
      });
    }
  }

  private locationInput: any;

  private async onSubmit() {
    this.setState({
      errors: {
        'trip.name': [''],
        'trip.location': [''],
      },
      isLoading: true,
    });

    const credentials = await Auth.getCredentials();

    if (!credentials) {
      return;
    }

    const response = await request(
      'trip/updateActiveTrip',
      'post',
      {
        trip: {
          name: this.state.name,
          location: this.state.location,
        },
      },
      credentials.password,
    );

    if (!response) {
      this.setState({
        isLoading: false,
      });

      return;
    }

    let responseJson;

    switch (response.status) {
      case 200:
        // all ok, trip was updated
        responseJson = await response.json();

        await Auth.setUserData(responseJson.user);

        showMessage('Trip Updated');

        this.props.navigation.navigate('Home');
        return;
      case 422:
        // validation errors
        responseJson = await response.json();

        this.setState({
          errors: responseJson.errors,
        });
        break;
      case 403:
        responseJson = await response.json();

        Alert.alert(
          'Error',
          responseJson.message,
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );

        break;
      default:
        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }

    this.setState({
      isLoading: false,
    });
  }

  private initDelete() {
    Alert.alert(
      'Confirm',
      'Delete the trip and all associated memories?',
      [
        {text: 'Cancel', onPress: () => {}},
        {
          text: 'Yes',
          onPress: () => this.props.navigation.navigate('DeleteTripLoading'),
        },
      ],
      {cancelable: true},
    );
  }

  render() {
    return (
      <View style={styles.mainView}>
        <ScrollView
          style={styles.scrollView}
          keyboardShouldPersistTaps="handled">
          <View style={styles.inputView}>
            <Input
              label="Trip Name"
              leftIcon={
                <Icon
                  name="suitcase"
                  type="font-awesome"
                  color={variables.inputIconColor}
                  size={variables.inputIconSize}
                />
              }
              leftIconContainerStyle={styles.inputIconStyle}
              value={this.state.name}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors['trip.name']
                  ? this.state.errors['trip.name'][0]
                  : ''
              }
              onChangeText={name => this.setState({name})}
              returnKeyType="next"
              onSubmitEditing={() => {
                this.locationInput.focus();
              }}
              blurOnSubmit={false}
              autoFocus={true}
            />
          </View>

          <View style={styles.inputView}>
            <Input
              ref={input => {
                this.locationInput = input;
              }}
              label="Location"
              leftIcon={
                <Icon
                  name="location-on"
                  type="material"
                  color={variables.inputIconColor}
                  size={variables.inputIconSize}
                />
              }
              leftIconContainerStyle={styles.inputIconStyle}
              value={this.state.location}
              errorStyle={styles.errorStyle}
              errorMessage={
                this.state.errors['trip.location']
                  ? this.state.errors['trip.location'][0]
                  : ''
              }
              onChangeText={location => this.setState({location})}
              returnKeyType="go"
              onSubmitEditing={() => {
                this.onSubmit();
              }}
            />
          </View>

          <View style={styles.buttonView}>
            <OutlineButton
              icon={
                <Icon
                  name="check"
                  type="material-community"
                  color={variables.lightGreen}
                  size={variables.inputIconSize}
                />
              }
              title="Update trip"
              onPress={() => this.onSubmit()}
              loading={this.state.isLoading}
            />

            <Button
              icon={
                <Icon
                  name="close"
                  type="material-community"
                  color={'white'}
                  size={variables.inputIconSize}
                />
              }
              buttonStyle={{
                marginTop: 20,
                backgroundColor: variables.endTripColor,
              }}
              iconContainerStyle={{marginRight: 10}}
              title="Delete trip"
              onPress={() => this.initDelete()}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
