import React, {Component} from 'react';
import {
  View,
  Alert,
  RefreshControl,
  Dimensions,
  StatusBar,
  Switch,
} from 'react-native';
import {Button, Icon, Text, ListItem} from 'react-native-elements';

import FastImage from 'react-native-fast-image';

import VariableColumnList from '../VariableColumnList';

import {NavigationEventSubscription} from 'react-navigation';
import {NavigationStackOptions} from 'react-navigation-stack';

import {NavigationStackProp} from 'react-navigation-stack';
import styles from '../../StyleSheets/Screens/HomeScreen';
import variables from '../../StyleSheets/variables';

import {
  setWhiteStatusBar,
  deleteFileIfExists,
  sortByDate,
  request,
  getColumnLayout,
  getColumnIndexOfResource,
} from '../../helpers';
import Auth from '../../Modules/Auth';
import PhotoManager from '../../Modules/PhotoManager';
import DiaryManager from '../../Modules/DiaryManager';

import Gallery from '../Gallery';

import {TouchableOpacity} from 'react-native-gesture-handler';
import {screensEnabled} from 'react-native-screens';
import Ad from '../../Components/Ad';
import {FirebaseAdMobTypes, BannerAdSize} from '@react-native-firebase/admob';

type HomeScreenProps = {
  navigation: NavigationStackProp;
};

function initResources() {
  return [{dummyFirstItem: true, key: 'dummyFirstItem'}];
}

export default class HomeScreen extends Component<HomeScreenProps> {
  state = {
    hasActiveTrip: undefined,
    private: false,
    isLoading: true,
    switchLoading: false,
    scrollPos: 0,
    showControls: true,
    columnLayout: [3],
    offlinePhotos: [],
    offlineResources: initResources(),
    galleryVisible: false,
    galleryModalVisible: false,
    galleryIndex: 0,
    isFocused: true,
    hasAds: true,
  };

  listRef: any;
  private photoDim:any;

  constructor(props: HomeScreenProps) {
    super(props);

    this.renderItem = this.renderItem.bind(this);
    this.onScroll = this.onScroll.bind(this);
  }

  static navigationOptions: NavigationStackOptions = {
    headerTitleContainerStyle: {justifyContent: 'center'},
  };

  private didFocusListener: NavigationEventSubscription | null = null;
  private didBlurListener: NavigationEventSubscription | null = null;
  private willFocusListener: NavigationEventSubscription | null = null;

  async componentDidMount() {
    this.photoDim = (Dimensions.get('window').width-30)/3;

    this.didFocusListener = this.props.navigation.addListener(
      'didFocus',
      () => {
        setWhiteStatusBar();
      },
    );

    this.didBlurListener = this.props.navigation.addListener('didBlur', () => {
      this.setState({isFocused: false});
    });

    this.willFocusListener = this.props.navigation.addListener(
      'willFocus',
      async () => {
        this.setState({isFocused: true});
        await this.performRefresh();
      },
    );

    await this.performRefresh();
  }

  componentWillUnmount() {
    this.didFocusListener!.remove();
    this.didBlurListener!.remove();
    this.willFocusListener!.remove();
  }

  private endTrip() {
    Alert.alert(
      'End Trip',
      'Please make sure you stay connected to the internet and do not close the application until you are redirected.',
      [
        {
          text: 'Cancel',
          onPress: () => {},
        },
        {
          text: 'OK',
          onPress: () => this.props.navigation.navigate('EndTripLoading'),
        },
      ],
      {cancelable: false},
    );
  }

  private async hasActiveTrip() {
    const user = await Auth.user();

    if (user) {
      this.setState({hasAds: user.has_ads});
    }

    if (user && user.has_active_trip) {
      return user.has_active_trip;
    }

    return false;
  }

  private async togglePublic() {
    if (!this.state.hasActiveTrip) {
      return;
    }

    let trip = this.state.hasActiveTrip;
    trip.private = !trip.private;
    this.setState({private: !this.state.private, hasActiveTrip: trip});

    const credentials = await Auth.getCredentials();

    if (!credentials) {
      return;
    }

    const response = await request(
      'trip/' + this.state.hasActiveTrip.id + '/togglePublic',
      'post',
      undefined,
      credentials.password,
    );

    if (!response) {
      let trip = this.state.hasActiveTrip;
      trip.private = !trip.private;
      this.setState({private: !this.state.private, hasActiveTrip: trip});

      return;
    }

    switch (response.status) {
      case 200:
        // all ok, trip was updated

        return;
      default:
        let trip = this.state.hasActiveTrip;
        trip.private = !trip.private;
        this.setState({private: !this.state.private, hasActiveTrip: trip});

        Alert.alert(
          'Error',
          'An unexpected error ocurred. Please try again later.',
          [{text: 'OK', onPress: () => {}}],
          {cancelable: false},
        );
    }
  }

  hideGallery() {
    this.setState({
      galleryVisible: false,
      galleryModalVisible: false,
      galleryIndex: 0,
    });
    this.listRef.scrollToIndex({
      index: getColumnIndexOfResource(
        this.state.columnLayout,
        this.state.offlinePhotos[this.state.galleryIndex].resourceIndex,
      ),
      animated: false,
    });
    StatusBar.setHidden(false);
  }

  private async performRefresh() {
    this.setState({isLoading: true});

    const trip = await this.hasActiveTrip();
    if (trip) {
      let photos = await PhotoManager.getLatestOfflinePhotos();
      let diaries = await DiaryManager.getOfflineDiaries();

      photos = photos?.map((value, index) => {
        value.photoIndex = index;
        return value;
      });

      photos = photos ? photos : [];
      diaries = diaries ? diaries : [];

      let resources = sortByDate(photos?.concat(diaries));
      resources =
        resources && resources.length > 0 ? resources : initResources();

      for (let [index, res] of resources.entries()) {
        if (res.photoIndex) {
          photos[res.photoIndex].resourceIndex = index;
        }
      }

      this.setState({
        isLoading: false,
        offlinePhotos: photos,
        offlineResources: resources,
        hasActiveTrip: trip,
        private: trip.private,
      });

      this.setColumnLayout();
    } else {
      this.setState({
        isLoading: false,
        offlinePhotos: [],
        offlineResources: initResources(),
        hasActiveTrip: false,
        private: false,
      });
    }
  }

  private setColumnLayout() {
    this.setState({columnLayout: getColumnLayout(this.state.offlineResources)});
  }

  private onScroll(e: {
    nativeEvent: {
      contentInset: {bottom: number; left: number; right: number; top: number};
      contentOffset: {x: number; y: number};
      contentSize: {height: number; width: number};
      layoutMeasurement: {height: number; width: number};
      zoomScale: number;
    };
  }) {
    // hide endTrip and Diary controls on scroll down
    const prev = this.state.scrollPos;
    const now = e.nativeEvent.contentOffset.y;

    if (now - prev > 10) {
      this.setState({
        showControls: false,
      });
    } else if (now - prev < -10) {
      this.setState({showControls: true});
    }

    this.setState({scrollPos: now});
  }

  private getPhotoSource(item: any) {
    return {
      uri: item.uri,
    };
  }

  private async deletePhoto() {
    const index = this.state.galleryIndex;
    const tempPhoto = this.state.offlinePhotos[index];

    this.setState({galleryVisible: false});
    await PhotoManager.removeOfflinePhoto(tempPhoto.uri);
    const filePath = tempPhoto.uri.split('///').pop();

    deleteFileIfExists(filePath);

    let temp = this.state.offlinePhotos;
    temp.splice(index, 1);
    this.setState({offlinePhotos: temp});

    this.performRefresh();

    if (this.state.offlinePhotos.length) {
      setTimeout(() => this.showGallery(), 1);
    } else {
      this.setState({galleryModalVisible: false});
      StatusBar.setHidden(false);
    }
  }

  private renderItem(itemObj: any) {
    const item = itemObj.item;

    if (item.dummyFirstItem) {
      return (
        <TouchableOpacity
          style={{
            margin: 10,
            padding: 10,
            width: (Dimensions.get('window').width - 60) / 2,
            height: (Dimensions.get('window').width - 60) / 2,
            borderRadius: 5,
            backgroundColor: variables.lightGreen,
          }}
          onPress={() => this.props.navigation.navigate('Camera')}>
          <View style={{flex: 1}}>
            <Text style={{flex: 2, fontSize: 22, color: 'white'}}>
              Start making memories
            </Text>
            <Icon
              containerStyle={{
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
              }}
              color="white"
              name="add"
              type="material"
              size={45}
            />
          </View>
        </TouchableOpacity>
      );
    }

    if (item.uri) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            marginBottom: 10,
          }}>
          <TouchableOpacity
            style={{
              width: this.photoDim,
              height: this.photoDim,
              alignSelf: 'center',
            }}
            onPress={() => {
              this.showGallery();
              this.setState({galleryIndex: item.photoIndex});
            }}>
            <FastImage
              style={{
                width: '100%',
                height: '100%',
                borderRadius: 5,
              }}
              source={this.getPhotoSource(item)}
            />
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            marginHorizontal: 5,
            marginBottom: 10,
          }}>
          <ListItem
            Component={TouchableOpacity}
            containerStyle={{
              borderRadius: 5,
              borderWidth: 1,
              borderColor: variables.darkGreen,
            }}
            onPress={() =>
              this.props.navigation.navigate({
                routeName: 'ViewDiary',
                params: {diary: item.uuid},
              })
            }
            title={item.title}
            titleProps={{numberOfLines:1}}
            subtitle={
              item.body
            }
            subtitleProps={{
              numberOfLines: 3
            }}
            leftAvatar={
              <Icon
                name="book-outline"
                type="material-community"
                color={variables.darkGreen}
              />
            }
            chevron={{color: variables.darkGreen, size: 25}}
          />
        </View>
      );
    }
  }

  private showGallery() {
    this.setState({galleryVisible: true, galleryModalVisible: true});
    StatusBar.setHidden(true);
  }

  render() {
    let content;

    if (!this.state.isFocused) {
      content = null;
    } else if (this.state.hasActiveTrip) {
      content = (
        <>
          <View style={{flex: 1}}>
            <VariableColumnList
              ref={(list) => (this.listRef = list)}
              initialNumToRender={10}
              windowSize={11}
              removeClippedSubviews={true}
              columnLayout={this.state.columnLayout}
              onScroll={this.onScroll}
              data={this.state.offlineResources}
              renderItem={this.renderItem}
              keyExtractor={(item) => (item.uri ? item.uri : item.uuid)}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={async () => await this.performRefresh()}
                />
              }
              ListHeaderComponent={
                <View style={{flexDirection: 'row', marginHorizontal:10, marginVertical:5}}>
                  <View style={{flex: 5}}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('EditTrip')
                      }>
                      <Text h3>{this.state.hasActiveTrip.name}</Text>
                      <Text style={{marginBottom: 10}}>
                        {this.state.hasActiveTrip.location}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 1, alignItems:'flex-end'}}>
                    <Text
                      style={{
                        fontSize: 17,
                        color: this.state.private
                          ? 'black'
                          : variables.darkGreen,
                      }}>
                      Public
                    </Text>
                    <Switch
                      disabled={this.state.switchLoading}
                      value={!this.state.private}
                      onValueChange={async () => {
                        this.setState({switchLoading: true});
                        await this.togglePublic();
                        this.setState({switchLoading: false});
                      }}
                    />
                  </View>
                </View>
              }
            />

            <Gallery
              canDeletePhoto={true}
              visible={this.state.galleryModalVisible}
              galleryVisible={this.state.galleryVisible}
              photos={this.state.offlinePhotos}
              onRequestClose={() => this.hideGallery()}
              onDeletePhoto={async () => await this.deletePhoto()}
              index={this.state.galleryIndex}
              onPageSelected={(index) => this.setState({galleryIndex: index})}
            />

            {this.state.showControls && (
              <>
                <View
                  style={{
                    position: 'absolute',
                    bottom: 5,
                    right: 7,
                  }}>
                  <Button
                    buttonStyle={{
                      backgroundColor: variables.lightGreen,
                      padding: 15,
                      paddingHorizontal: 25,
                      borderRadius: 30,
                      elevation: 5,
                    }}
                    icon={
                      <Icon
                        type="font-awesome"
                        name="pencil"
                        color="white"
                        size={variables.inputIconSize}
                      />
                    }
                    titleStyle={{marginHorizontal: 5}}
                    title="Diary"
                    onPress={() => {
                      this.props.navigation.navigate('NewDiary');
                    }}
                  />
                </View>

                <View
                  style={{
                    position: 'absolute',
                    bottom: 5,
                    left: 7,
                  }}>
                  <Button
                    buttonStyle={{
                      backgroundColor: 'white',
                      padding: 15,
                      paddingHorizontal: 25,
                      borderRadius: 30,
                      borderColor: variables.endTripColor,
                      borderWidth: 1,
                    }}
                    raised
                    titleStyle={{
                      marginHorizontal: 5,
                      color: variables.endTripColor,
                    }}
                    title="Finish Trip"
                    onPress={() => {
                      this.endTrip();
                    }}
                  />
                </View>
              </>
            )}
          </View>
        </>
      );
    } else if (this.state.hasActiveTrip === false) {
      content = (
        <TouchableOpacity
          style={{
            margin: 10,
            padding: 10,
            width: (Dimensions.get('window').width - 60) / 2,
            height: (Dimensions.get('window').width - 60) / 2,
            borderRadius: 5,
            backgroundColor: variables.lightGreen,
          }}
          onPress={() => this.props.navigation.navigate('NewTrip')}>
          <View style={{flex: 1}}>
            <Text style={{flex: 2, fontSize: 22, color: 'white'}}>
              Start a new trip
            </Text>
            <Icon
              containerStyle={{
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
              }}
              color="white"
              name="add"
              type="material"
              size={45}
            />
          </View>
        </TouchableOpacity>
      );
    }

    return (
      <>
        <View style={styles.mainView}>{content}</View>
        <Ad
          hasAds={this.state.hasAds}
          id="ca-app-pub-3943805809685999/4948872211"
          size={BannerAdSize.SMART_BANNER}
        />
      </>
    );
  }
}
