import React, {Component} from 'react';
import {ScrollView, View, Text} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import variables from '../../StyleSheets/variables';
import {NavigationStackProp} from 'react-navigation-stack';
import {NavigationEventSubscription} from 'react-navigation';

import styles from '../../StyleSheets/Screens/ViewDiaryScreen';

import DiaryManager from '../../Modules/DiaryManager';

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';

type ViewDiaryScreenProps = {
  navigation: NavigationStackProp;
};

export default class ViewDiaryScreen extends Component<ViewDiaryScreenProps> {
  static navigationOptions = ({navigation}) => {
    return {
      tabBarVisible: false,
      headerTitle: () => null,
      headerStyle: {
        elevation: 0,
      },
      headerRight: () => (
        <View style={{marginRight: 15}}>
          <Menu>
            <MenuTrigger>
              <Icon
                color={variables.darkGreen}
                name="dots-vertical"
                type="material-community"
                size={27}
              />
            </MenuTrigger>
            <MenuOptions
              customStyles={{
                optionText: {fontSize: 15},
                optionWrapper: {padding: 10},
              }}>
              <MenuOption
                onSelect={async () => {
                  const func = navigation.getParam('onEdit');
                  await func();
                }}
                text="Edit"
              />
              <MenuOption
                onSelect={async () => {
                  const func = navigation.getParam('onDelete');
                  await func();
                }}
                text="Delete"
              />
            </MenuOptions>
          </Menu>
        </View>
      ),
    };
  };

  private willFocusListener: NavigationEventSubscription | null = null;

  state = {
    isLoading: false,
    diary: undefined,
  };

  async componentDidMount() {
    this.props.navigation.setParams({
      onDelete: this.onDelete,
      onEdit: this.onEdit,
    });

    this.willFocusListener = this.props.navigation.addListener(
      'willFocus',
      async () => {
        if (this.props.navigation.getParam('refresh') === true) {
          await this.performRefresh();

          this.props.navigation.setParams({refresh: undefined});
        }
      },
    );

    await this.performRefresh();
  }

  async performRefresh() {
    const diary = await DiaryManager.getOfflineDiary(
      this.props.navigation.getParam('diary'),
    );

    if (!diary) {
      return;
    }

    this.setState({diary: diary});
  }

  componentWillUnmount() {
    this.setState({diary: undefined});

    this.willFocusListener!.remove();

    this.props.navigation.setParams({
      onDelete: undefined,
      onEdit: undefined,
      diary: undefined,
    });
  }

  onDelete = async () => {
    await DiaryManager.removeOfflineDiary(this.state.diary.uuid);

    this.props.navigation.navigate({routeName: 'Home'});
  };

  onEdit = async () => {
    this.props.navigation.navigate({
      routeName: 'EditDiary',
      params: {diary: this.state.diary.uuid},
    });
  };

  render() {
    if (this.state.diary) {
      return (
        <>
          <View style={styles.titleView}>
            <Text style={styles.titleText}>{this.state.diary.title}</Text>
          </View>
          <View style={styles.mainView}>
            <ScrollView
              style={styles.scrollView}
              keyboardShouldPersistTaps="handled">
              <View style={styles.bodyView}>
                <Text>{this.state.diary.body}</Text>
              </View>
            </ScrollView>
          </View>
        </>
      );
    }

    return <></>;
  }
}
