import React, {Component} from 'react';
import {View, Text, TouchableOpacity, ShadowPropTypesIOS} from 'react-native';
import {Icon} from 'react-native-elements';
import variables from '../StyleSheets/variables';

type OfflineViewProps = {
  onPress: () => void;
  title: string;
};

export default (props: OfflineViewProps) => {
  return (
    <View style={[{alignItems: 'center', justifyContent: 'center'}]}>
      <TouchableOpacity onPress={props.onPress}>
        <Icon
          type="material"
          name="cloud-off"
          size={45}
          color={variables.lightGreen}
        />

        <Text style={{color: variables.lightGreen, fontSize: 25}}>
          {props.title}
        </Text>
      </TouchableOpacity>
    </View>
  );
};
