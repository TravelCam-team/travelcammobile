import React from 'react';
import {ListItem, Icon} from 'react-native-elements';
import {View, Text} from 'react-native';
import variables from '../StyleSheets/variables';

type TripListItemProps = {
  onPress: () => void;
  item: any;
};

export default (props: TripListItemProps) => {
  return (
    <ListItem
      title={props.item.name}
      subtitle={
        <View style={{flexDirection: 'row', marginTop: 5}}>
          <Icon
            name="location-on"
            type="material"
            color={variables.grey}
            size={19}
          />
          <Text style={{color: variables.grey}}>{props.item.location}</Text>
        </View>
      }
      bottomDivider
      chevron
      onPress={props.onPress}
    />
  );
};
