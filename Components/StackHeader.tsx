import React, {Component} from 'react';
import FastImage from 'react-native-fast-image';
import {View, Text} from 'react-native';
import variables from '../StyleSheets/variables';

type StackHeaderProps = {
  title?: string | undefined;
};

export default (props: StackHeaderProps) => {
  if (props.title) {
    return (
      <View>
        <Text style={{color: variables.darkGreen, fontSize: 19}}>
          {props.title}
        </Text>
      </View>
    );
  }

  return (
    <View style={{flexDirection: 'row'}}>
      <FastImage
        style={{width: 25, height: 25, marginRight: 10}}
        source={require('../img/logo.png')}
      />
      <Text style={{color: variables.darkGreen, fontSize: 19}}>TravelCam</Text>
    </View>
  );
};
