import {
  BannerAd,
  TestIds,
  FirebaseAdMobTypes,
} from '@react-native-firebase/admob';

import React, {Component} from 'react';
import {View, Dimensions} from 'react-native';

type AdProps = {
  id: string;
  hasAds: boolean;
  size: FirebaseAdMobTypes.BannerAdSize;
  isMediumRectangle?: boolean | undefined;
};

export default class Ad extends Component<AdProps> {
  maxHeight: any;

  constructor(props: AdProps) {
    super(props);

    if (!this.props.isMediumRectangle) {
      const height = Dimensions.get('screen').height;

      if (height <= 400) {
        this.maxHeight = 32;
      } else if (height <= 720) {
        this.maxHeight = 50;
      } else {
        this.maxHeight = 90;
      }
    }
  }

  render() {
    if (!this.props.hasAds) {
      return null;
    }

    const unitId = __DEV__ ? TestIds.BANNER : this.props.id;

    if (!this.props.isMediumRectangle) {
      return (
        <View style={{maxHeight: this.maxHeight}}>
          <BannerAd unitId={unitId} size={this.props.size} />
        </View>
      );
    }

    return (
      <View style={{width: 300, height: 250}}>
        <BannerAd unitId={unitId} size={this.props.size} />
      </View>
    );
  }
}
