import React from 'react';
import FastImage, {FastImageSource} from 'react-native-fast-image';
import {TouchableOpacity, View, Text, Dimensions} from 'react-native';
import {Icon} from 'react-native-elements';

type TripThumbnailProps = {
  onPress: () => void;
  item: any;
  source?: FastImageSource | undefined;
  hasThumbnail: boolean;
};

export default (props: TripThumbnailProps) => {
  const width = (Dimensions.get('window').width - 60) / 2;

  return (
    <View style={{borderRadius: 5, margin: 10}}>
      <TouchableOpacity style={{width}} onPress={props.onPress}>
        {props.hasThumbnail && props.source ? (
          <FastImage
            source={props.source}
            style={{
              width,
              height: width,
              borderTopLeftRadius: 5,
              borderTopRightRadius: 5,
            }}
          />
        ) : (
          <View
            style={{
              width,
              height: width,
              borderTopLeftRadius: 5,
              borderTopRightRadius: 5,
              backgroundColor: 'rgba(75, 191, 107, 0.9)',
            }}
          />
        )}
        <View
          style={{
            padding: 5,
            backgroundColor: 'rgba(75, 191, 107, 0.9)',
            borderBottomLeftRadius: 5,
            borderBottomRightRadius: 5,
            overflow: 'hidden',
          }}>
          <Text numberOfLines={1} style={{fontSize: 20}}>
            {props.item.name}
          </Text>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Icon
              name="location-on"
              type="material"
              color="rgba(0,0,0, 0.7)"
              size={20}
            />

            <Text numberOfLines={1} style={{color: 'rgba(0,0,0, 0.7)'}}>
              {props.item.location}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};
