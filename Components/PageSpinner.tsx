import React from 'react';
import {ActivityIndicator} from 'react-native';

import styles from '../StyleSheets/PageSpinner';

export default (props: any) => {
  return (
    <ActivityIndicator
      {...props}
      size={styles.spinnerSize}
      color={styles.spinnerColor}
    />
  );
};
