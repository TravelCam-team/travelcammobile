import React from 'react';

import {Button, ButtonProps} from 'react-native-elements';

import styles from '../StyleSheets/OutlineButton';

export default (props: ButtonProps) => {
  return (
    <Button
      type="outline"
      title={props.title}
      onPress={props.onPress}
      icon={props.icon}
      loading={props.loading}
      disabled={props.loading}
      disabledStyle={styles.disabledStyle}
      buttonStyle={styles.button}
      titleStyle={styles.buttonTitle}
      loadingProps={styles.loadingProps}
    />
  );
};
