import React from 'react';
import {StatusBar} from 'react-native';

export default () => {
  return (
    <StatusBar
      backgroundColor="white"
      translucent={false}
      barStyle="dark-content"
    />
  );
};
